using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Mock.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Adapters.Mock.Tests.Services
{
    /// <summary>
    /// Unit tests for MockAdapter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MockAdapterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Expect to receive response message from matched condition
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithOneConditionMatch()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = ">",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", (inputBaseNum + 1).ToString()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"exception\":\"{2}\"}}],\"default\":{{\"response\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.response, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to receive response message from the first matched condition
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithTwoConditionsMatch()
        {
            #region Setup
            string inputBaseNum = faker.Random.Int().ToString();
            var input = new
            {
                op = "==",
                comapreTo = inputBaseNum,
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", inputBaseNum),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"response\":\"{2}\"}}, {{\"operator\":\"{3}\",\"compareTo\":\"{4}\",\"response\":\"{5}\"}}],\"default\":{{\"response\":\"{6}\"}}}}",
                input.op, input.comapreTo, input.response, input.op, input.comapreTo, input.wrongResponse, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive response message from the matched condition
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithLastConditionsMatch()
        {
            #region Setup
            string inputBaseNum = faker.Random.Int().ToString();
            var input = new
            {
                comapreTo = inputBaseNum,
                response = string.Format("{{\"{0}\":\"{1}\"}}", faker.Lorem.Word(), faker.Lorem.Word()),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", false),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\">=\",\"compareTo\":\"{0}\",\"response\":\"{1}\"}}, {{\"operator\":null,\"compareTo\":\"{2}\",\"response\":\"{3}\"}}, {{\"operator\":\"<=\",\"compareTo\":\"{4}\",\"response\":\"{5}\"}}, {{\"operator\":\"!=\",\"compareTo\":\"{6}\",\"response\":{7}}}],\"default\":{{\"response\":\"{8}\"}}}}",
                input.comapreTo, input.wrongResponse, input.comapreTo, input.wrongResponse, input.comapreTo, input.wrongResponse, true, input.response, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive response message from default value
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithNoConditionMatchResponse()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = "<",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", faker.Random.String()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"response\":\"{2}\"}}],\"default\":{{\"response\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.wrongResponse, input.response);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive fault message from default value
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithNoConditionMatchError()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = "<",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", faker.Random.String()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"response\":\"{2}\"}}],\"default\":{{\"exception\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.wrongResponse, input.response);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to receive default response, since compareTo value isn't provided
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithNoCompareTo()
        {
            #region Setup
            string inputBaseNum = faker.Random.Int().ToString();
            var input = new
            {
                op = "==",
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", inputBaseNum),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":null,\"response\":\"{1}\"}}],\"default\":{{\"exception\":\"{2}\"}}}}",
                input.op, input.response, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.wrongResponse;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to receive default response, since condition isn't provided
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncWithNoCondition()
        {
            #region Setup
            string inputBaseNum = faker.Random.Int().ToString();
            var input = new
            {
                op = "==",
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", inputBaseNum),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"default\":{{\"response\":\"{0}\"}}}}", input.response);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive fault with correct message
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithNullRequest()
        {
            var Target = new MockAdapter();

            var Expected = "Adapter request cannot be null";
            var Actual = Target.SendSync(null);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to receive fault with correct message
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithNullConfiguration()
        {
            #region Setup
            var input = new
            {
                message = string.Format("{{\"id\":\"{0}\"}}", faker.Random.String()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            AdapterRequest inputRequest = new AdapterRequest(string.Empty, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = "Unable to read adapter configuration ()";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to receive fault with correct message
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncNullSelector()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = ">",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = "{}",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":null,\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"exception\":\"{2}\"}}],\"default\":{{\"response\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.wrongResponse, input.response);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive fault with correct message
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncInvalidRequest()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = ">",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = "",
                param = new Dictionary<string, string>()
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"exception\":\"{2}\"}}],\"default\":{{\"response\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.wrongResponse, input.response);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive fault with correct message
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncUnableToGetSelectedValue()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = ">",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = "{}",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"exception\":\"{2}\"}}],\"default\":{{\"response\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.wrongResponse, input.response);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => r, e => string.Empty));
        }

        /// <summary>
        /// Expect to receive fault message indicating matched condition did not provide a response
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncConditionMatchedNoResponse()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = ">",
                comapreTo = inputBaseNum.ToString(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", (inputBaseNum + 1).ToString()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\"}}],\"default\":{{\"response\":\"{2}\"}}}}",
                input.op, input.comapreTo, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = "One condition matched, but exception message isn't provided";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }



        /// <summary>
        /// Expect to receive fault message indicating default value isn't provided
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithNoDefault()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = "<",
                comapreTo = inputBaseNum.ToString(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", faker.Random.String()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"response\":\"{2}\"}}]}}",
                input.op, input.comapreTo, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = "The default value is required because the selected value did not match any condition";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to receive fault message indicating default did not provide a response
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithNoDefaultValue()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = "<",
                comapreTo = inputBaseNum.ToString(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", faker.Random.String()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"response\":\"{2}\"}}],\"default\":{{}}}}",
                input.op, input.comapreTo, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = "Default exception message isn't provided";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// Expect to send async to run without fail
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendAsyncSuccess()
        {
            #region Setup
            int inputBaseNum = faker.Random.Int();
            var input = new
            {
                op = ">",
                comapreTo = inputBaseNum.ToString(),
                response = faker.Lorem.Word(),
                wrongResponse = faker.Lorem.Word(),
                message = string.Format("{{\"id\":\"{0}\"}}", (inputBaseNum + 1).ToString()),
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            string inputConfig = string.Format("{{\"selector\":\"$.id\",\"conditions\":[{{\"operator\":\"{0}\",\"compareTo\":\"{1}\",\"exception\":\"{2}\"}}],\"default\":{{\"response\":\"{3}\"}}}}",
                input.op, input.comapreTo, input.response, input.wrongResponse);
            AdapterRequest inputRequest = new AdapterRequest(inputConfig, input.message, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new MockAdapter();

            var Expected = input.response;
            Target.SendAsync(inputRequest);
        }
    }
}
