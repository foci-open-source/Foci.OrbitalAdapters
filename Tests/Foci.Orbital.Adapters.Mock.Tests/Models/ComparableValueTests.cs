using Bogus;
using Foci.Orbital.Adapters.Mock.Models;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Mock.Tests.Models
{
    /// <summary>
    /// Unit tests for ComparableValue class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ComparableValueTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Expect to receive true when two boolean is provided
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void IsSameTypeWithTwoBool()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(false.ToString());

            Assert.True(Target1.IsSameType(Target2));
        }

        /// <summary>
        /// Expect to receive true when two double is provided
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void IsSameTypeWithTwoDouble()
        {
            var Target1 = new ComparableValue(faker.Random.Double().ToString());
            var Target2 = new ComparableValue(faker.Random.Double().ToString());

            Assert.True(Target1.IsSameType(Target2));
        }

        /// <summary>
        /// Expect to receive true when two string is provided
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void IsSameTypeWithTwoString()
        {
            var Target1 = new ComparableValue(faker.Random.String());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.True(Target1.IsSameType(Target2));
        }

        /// <summary>
        /// Expect to receive false when a bool and a double is provided
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSameTypeWithBoolAndDouble()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.Double().ToString());

            Assert.False(Target1.IsSameType(Target2));
        }

        /// <summary>
        /// Expect to receive false when a bool and a string is provided
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSameTypeWithBoolAndString()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target1.IsSameType(Target2));
        }

        /// <summary>
        /// Expect to receive false when a double and a string is provided
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSameTypeWithDoubleAndString()
        {
            var Target1 = new ComparableValue(faker.Random.Double().ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target1.IsSameType(Target2));
        }

        /// <summary>
        /// Expect to receive correct number when comparing booleans
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CompareToWithBools()
        {
            var Target1 = new ComparableValue(false.ToString());
            var Target2 = new ComparableValue(true.ToString());

            //Expect less than
            var Expected = -1;
            var Actual = Target1.CompareTo(Target2);

            Assert.Equal(Expected, Actual);

            //Expect greater than
            Expected = 1;
            Actual = Target2.CompareTo(Target1);

            Assert.Equal(Expected, Actual);

            //Expect equal
            Expected = 0;
            Actual = Target1.CompareTo(Target1);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Expect to receive correct number when comparing doubles
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CompareToWithDoubles()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            var Target2 = new ComparableValue(faker.Random.Double(101, 200).ToString());

            //Expect less than
            var Expected = -1;
            var Actual = Target1.CompareTo(Target2);

            Assert.Equal(Expected, Actual);

            //Expect greater than
            Expected = 1;
            Actual = Target2.CompareTo(Target1);

            Assert.Equal(Expected, Actual);

            //Expect equal
            Expected = 0;
            Actual = Target1.CompareTo(Target1);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Expect to receive correct number when comparing strings
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CompareToWithStrings()
        {
            var Target1 = new ComparableValue("a");
            var Target2 = new ComparableValue("b");

            //Expect less than
            var Expected = -1;
            var Actual = Target1.CompareTo(Target2);

            Assert.Equal(Expected, Actual);

            //Expect greater than
            Expected = 1;
            Actual = Target2.CompareTo(Target1);

            Assert.Equal(Expected, Actual);

            //Expect equal
            Expected = 0;
            Actual = Target1.CompareTo(Target1);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Expect compareTo to compare the different value as if they are strings
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void CompareToWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue("b");

            var Expected = 1;
            var Actual = Target1.CompareTo(Target2);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void LTWithSameType()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            var Target2 = new ComparableValue(faker.Random.Double(101, 200).ToString());

            Assert.True(Target1 < Target2);
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void LTWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target1 < Target2);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GTWithSameType()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            var Target2 = new ComparableValue(faker.Random.Double(101, 200).ToString());

            Assert.True(Target2 > Target1);
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GTWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target2 > Target1);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void EQWithSameType()
        {
            var input = faker.Random.Double(-100, 100).ToString();
            var Target1 = new ComparableValue(input);
            var Target2 = new ComparableValue(input);

            Assert.True(Target1 == Target2);
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void EQWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target1 == Target2);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void NEWithSameType()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            var Target2 = new ComparableValue(faker.Random.Double(101, 200).ToString());

            Assert.True(Target1 != Target2);
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void NEWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target2 != Target1);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GEWithSameType()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            var Target2 = new ComparableValue(faker.Random.Double(100, 200).ToString());

            Assert.True(Target2 >= Target1);
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GEWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.Double(22222, 99999).ToString());

            Assert.False(Target2 >= Target1);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void LEWithSameType()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            var Target2 = new ComparableValue(faker.Random.Double(100, 200).ToString());

            Assert.True(Target1 <= Target2);
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void LEWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target1 <= Target2);
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void EqualsWithSameTypeSameValue()
        {
            var input = faker.Random.Double(-100, 100).ToString();
            var Target1 = new ComparableValue(input);
            object Target2 = new ComparableValue(input);

            Assert.True(Target1.Equals(Target2));
        }

        /// <summary>
        /// Expect to run as expected when comparing value that is the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void EqualsWithSameTypeDifferentValue()
        {
            var Target1 = new ComparableValue(faker.Random.Double(-100, 100).ToString());
            object Target2 = new ComparableValue(faker.Random.Double(100, 200).ToString());

            Assert.False(Target1.Equals(Target2));
        }

        /// <summary>
        /// Expect to return false when comparing value that is not the same type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void EqualsWithDifferentType()
        {
            var Target1 = new ComparableValue(true.ToString());
            var Target2 = new ComparableValue(faker.Random.String());

            Assert.False(Target1.Equals(Target2));
        }

        /// <summary>
        /// Expect to receive hash code for this class
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetHashCodeWithValidInput()
        {
            var input = "constant value";

            var Target = new ComparableValue(input);

            var Expected = new ComparableValue(input).GetHashCode();
            var Actual = Target.GetHashCode();

            Assert.Equal(Expected, Actual);
        }
    }
}
