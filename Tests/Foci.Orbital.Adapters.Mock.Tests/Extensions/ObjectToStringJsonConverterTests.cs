using Bogus;
using Foci.Orbital.Adapters.Mock.Extensions;
using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Mock.Tests.Extensions
{
    /// <summary>
    /// Unit tests for ObjectToStringJsonConverter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ObjectToStringJsonConverterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Expect the converter to be able to read and not able to write
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanReadIsTrue()
        {
            var Target = new ObjectToStringJsonConverter();

            Assert.True(Target.CanRead);
            Assert.False(Target.CanWrite);
        }

        /// <summary>
        /// Testing CanConvert with correct type
        /// </summary>
        [Theory]
        [InlineData(typeof(string))]
        [InlineData(typeof(int))]
        [InlineData(typeof(object))]
        [Trait("Category", "Success")]
        public void CanConvertWithValidType(Type type)
        {
            var Target = new ObjectToStringJsonConverter();
            Assert.True(Target.CanConvert(type));
        }

        /// <summary>
        /// Test deserializing various json objects to string
        /// </summary>
        [Theory]
        [InlineData("{\"Result\":\"a\"}", "a")]
        [InlineData("{\"Result\":1}", "1")]
        [InlineData("{\"Result\":{\"test\":\"test\"}}", "{\"test\":\"test\"}")]
        [Trait("Category", "Success")]
        public void ReadJsonWithLTString(string input, string exptected)
        {
            var Actual = JsonConvert.DeserializeObject<Test>(input);
            var Expected = exptected;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// WriteJson should throw NotImplementedException
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WriteJsonShoudlBeUndefined()
        {
            var Target = new ObjectToStringJsonConverter();
            Assert.Throws<NotImplementedException>(() => Target.WriteJson(null, null, null));
        }

        /// <summary>
        /// Testing class used to test ObjectToStringJsonConverter
        /// </summary>
        [ExcludeFromCodeCoverage]
        public class Test
        {
            [JsonConverter(typeof(ObjectToStringJsonConverter))]
            public string Result { get; set; }
        }
    }
}
