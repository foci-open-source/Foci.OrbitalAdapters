using Bogus;
using Foci.Orbital.Adapters.Mock.Extensions;
using Foci.Orbital.Adapters.Mock.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Mock.Tests.Extensions
{
    /// <summary>
    /// Unit tests for StringOperatorJsonConverter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class StringOperatorJsonConverterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Expect the converter to be able to read and not able to write
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanReadIsTrue()
        {
            var Target = new StringOperatorJsonConverter();

            Assert.True(Target.CanRead);
            Assert.False(Target.CanWrite);
        }

        /// <summary>
        /// Testing CanConvert with correct type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanConvertWithValidType()
        {
            var Target = new StringOperatorJsonConverter();

            var input = typeof(string);

            Assert.True(Target.CanConvert(input));
        }

        /// <summary>
        /// Testing CanConvert with invalid type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void CanConvertWithInvalidType()
        {
            var Target = new StringOperatorJsonConverter();

            var input = typeof(int);

            Assert.False(Target.CanConvert(input));
        }

        /// <summary>
        /// Test deserializing == to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithEQString()
        {
            var Input = "{\"Result\":\"==\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.EQ;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing != to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithNEString()
        {
            var Input = "{\"Result\":\"!=\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.NE;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing &gt; to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithGTString()
        {
            var Input = "{\"Result\":\">\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.GT;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing &lt; to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithLTString()
        {
            var Input = "{\"Result\":\"<\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.LT;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing &gt;= to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithGEString()
        {
            var Input = "{\"Result\":\">=\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.GE;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing &lt;= to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithLEString()
        {
            var Input = "{\"Result\":\"<=\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.LE;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing string to proper accepted operators enum
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonWithNullReader()
        {
            var Target = new StringOperatorJsonConverter();

            var Actual = Target.ReadJson(JObject.Parse("{}").CreateReader(), null, null, null);
            var Expected = AcceptedOperators.UNDEFINED;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test deserializing invalid string to AcceptedOperators.UNDEFINED
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonWithInvalidInput()
        {
            var Input = "{\"Result\":\"" + faker.Random.String() + "\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = AcceptedOperators.UNDEFINED;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// WriteJson should throw NotImplementedException
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WriteJsonShoudlBeUndefined()
        {
            var Target = new StringOperatorJsonConverter();
            Assert.Throws<NotImplementedException>(() => Target.WriteJson(null, null, null));
        }

        /// <summary>
        /// Testing class used to test StringOperatorJsonConverter
        /// </summary>
        [ExcludeFromCodeCoverage]
        public class Test
        {
            [JsonConverter(typeof(StringOperatorJsonConverter))]
            public AcceptedOperators Result { get; set; }
        }

    }
}
