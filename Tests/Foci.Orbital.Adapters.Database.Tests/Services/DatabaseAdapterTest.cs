﻿using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Database.DBClients.Interfaces;
using Foci.Orbital.Adapters.Database.Factories.Interfaces;
using Foci.Orbital.Adapters.Database.Models;
using Foci.Orbital.Adapters.Database.Services;
using NSubstitute;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Adapters.Database.Tests.Services
{
    public class DatabaseAdapterTest
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// To ensure that running a query sql statement with parameters, the fetched result are expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunQueryWithParameterSuccess()
        {
            #region Setup

            var input = new
            {
                statement = Statement.QUERY,
                sql = "select * from Customer where name = @name",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "Saul Goodman"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"name\": \"Goodman\"}}}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClient = Substitute.For<IDatabaseClient>();
            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(Payload.Create(input.result));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Actual = Target.SendSync(adapterRequest);

            var Expected = input.result;

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        /// <summary>
        /// To ensure that running a query sql statement without parameters, the fetched result are expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunQueryWithEmptyParameterSuccess()
        {
            #region Setup

            var input = new
            {
                statement = Statement.QUERY,
                sql = "select * from Customer",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>() { },
                result = "Saul Goodman"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\"}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));


            var dbClient = Substitute.For<IDatabaseClient>();
            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(Payload.Create(input.result));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Actual = Target.SendSync(adapterRequest);

            var Expected = input.result;

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        /// <summary>
        /// To ensure that running a execution sql statement, the returned result from database is as expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunExecutionWithParameterSuccess()
        {
            #region Setup

            var input = new
            {
                statement = Statement.EXECUTE,
                sql = "INSERT INTO `Customer` ( CustomerId, LastName, FirstName ) VALUES ( @id, @lastName, @firstName );",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "1 row affected"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"id\": \"1\", \"lastName\": \"Goodman\", \"firstName\": \"Saul\"}}}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClient = Substitute.For<IDatabaseClient>();

            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(Payload.Create(input.result));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Actual = Target.SendSync(adapterRequest);

            var Expected = input.result;

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        /// <summary>
        /// To ensure that running an Async execution sql statement and the returned result from database is as expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunAsyncExecutionWithParameterSuccess()
        {
            #region Setup

            var input = new
            {
                statement = Statement.EXECUTE,
                sql = "INSERT INTO `Customer` (`name`) VALUES( @name )",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "1 row affected"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"name\": \"Goodman\"}}}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClient = Substitute.For<IDatabaseClient>();

            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(Payload.Create(input.result));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            Target.SendAsync(adapterRequest);
        }


        /// <summary>
        /// To ensure that running a execution sql statement, the returned result from database is as expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunExecutionWithEmptyParameterSuccess()
        {
            #region Setup

            var input = new
            {
                statement = Statement.EXECUTE,
                sql = "INSERT INTO `Customer` (`name`) VALUES( Goodman )",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "1 row affected"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\"}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClient = Substitute.For<IDatabaseClient>();

            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(Payload.Create(input.result));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Actual = Target.SendSync(adapterRequest);

            var Expected = input.result;

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        /// <summary>
        /// To ensure that if the database has internal error, the expected fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RunQueryWithDatabaseFailure()
        {
            #region Setup

            var input = new
            {
                statement = Statement.EXECUTE,
                sql = "INSERT INTO `Customer` (`name`) VALUES( @name )",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "The database is broken!"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"name\": \"Goodman\"}}}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClient = Substitute.For<IDatabaseClient>();

            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(CreateFault("The database is broken!"));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Actual = Target.SendSync(adapterRequest);

            var Expected = input.result;

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that if the SQL syntax has error the expected fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RunQueryWithSyntaxError()
        {
            #region Setup

            var input = new
            {
                sql = "INSERTTTTTT INTO `Customer` (`name`) VALUES( @name )",
                statement = Statement.EXECUTE,
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "SQL syntax error!"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"name\": \"Goodman\"}}}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClient = Substitute.For<IDatabaseClient>();

            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(CreateFault("SQL syntax error!"));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);

            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Actual = Target.SendSync(adapterRequest);

            var Expected = input.result;

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that running a execution sql statement, the returned result from database is as expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithMustacheTemplate()
        {
            #region Substitute
            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            #endregion

            #region Setup
            var input = new
            {
                statement = Statement.EXECUTE,
                sql = "INSERT INTO `{{test}}` ( CustomerId, LastName, FirstName ) VALUES ( @id, @lastName, @firstName );",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                },
                result = "1 row affected"
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"id\": \"1\", \"lastName\": \"Goodman\", \"firstName\": \"Saul\"}}}}",
                input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));
            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Expected = "SQL statement cannot contain mustache template";
            var Actual = Target.SendSync(adapterRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that running a execution sql statement, the returned result from database is as expected.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithEmptySQL()
        {
            #region Substitute
            var dbClient = Substitute.For<IDatabaseClient>();
            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            #endregion

            #region Setup
            var input = new
            {
                statement = Statement.EXECUTE,
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"\", \"connection\":\"{2}\", \"parameters\": {{\"id\": \"1\", \"lastName\": \"Goodman\", \"firstName\": \"Saul\"}}}}",
                input.statement, input.databaseType, input.connection);

            AdapterRequest adapterRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            dbClient.Execute(input.statement, string.Empty, Arg.Any<Dictionary<string, string>>()).Returns(CreateFault("SQL syntax error!"));
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);
            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Expected = "SQL syntax error!";
            var Actual = Target.SendSync(adapterRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that if the configuration is empty string, the appropriate fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void EmptyConfigurationFailureCase()
        {
            #region Setup
            var input = new
            {
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };
            AdapterRequest inputRequest = new AdapterRequest(string.Empty, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Expected = "Unable to read adapter configuration";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that if the connection string is empty string, the appropriate fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void EmptyConnectionStringFailureCase()
        {
            #region Setup
            var input = new
            {
                sql = "INSERT INTO `Customer` (`name`) VALUES( @name )",
                statement = Statement.EXECUTE,
                databaseType = DatabaseType.MYSQL,
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };

            var adapterConfiguration = string.Format(
            "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\"}}",
            input.statement, input.databaseType, input.sql, string.Empty);

            AdapterRequest inputRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Expected = "Unable to connect to the database MYSQL with connection string: ";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that if the database type is undefined, the appropriate fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void UnknownDatabaseTypeFailureCase()
        {
            #region Setup
            var input = new
            {
                sql = "INSERT INTO `Customer` (`name`) VALUES( @name )",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                statement = Statement.EXECUTE,
                databaseType = DatabaseType.UNDEFINED,
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"name\": \"Goodman\"}}}}",
            input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest inputRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));

            #endregion

            var Target = new DatabaseAdapter();

            var Expected = "Unable to connect to the database UNDEFINED with connection string: Server=localhost;Database=Customer;Uid=root;Pwd=example";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that if the statement type is undefined, the appropriate fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void UndefinedStatementTypeFailureCase()
        {
            #region Setup
            var input = new
            {
                sql = "INSERT INTO `Customer` (`name`) VALUES( @name )",
                connection = "Server=localhost;Database=Customer;Uid=root;Pwd=example",
                statement = Statement.EXECUTE,
                databaseType = "MYSQL",
                param = new Dictionary<string, string>()
                {
                    { faker.Lorem.Word(), faker.Lorem.Word() }
                }
            };

            var adapterConfiguration = string.Format(
                "{{\"statement\":\"{0}\",\"type\":\"{1}\", \"sql\": \"{2}\", \"connection\":\"{3}\", \"parameters\": {{\"name\": \"Goodman\"}}}}",
            input.statement, input.databaseType, input.sql, input.connection);

            AdapterRequest inputRequest = new AdapterRequest(adapterConfiguration, null, new ReadOnlyDictionary<string, string>(input.param));
            var dbClient = Substitute.For<IDatabaseClient>();

            dbClient.Execute(input.statement, input.sql, Arg.Any<Dictionary<string, string>>()).Returns(CreateFault(string.Format("Unable to run the {0} statement: {1}", input.statement, input.sql)));
            var dbClientFactory = Substitute.For<IDatabaseClientFactory>();
            dbClientFactory.getClient(DatabaseType.MYSQL, input.connection).Returns(dbClient);
            #endregion

            var Target = new DatabaseAdapter(dbClientFactory);

            var Expected = "Unable to run the EXECUTE statement: INSERT INTO `Customer` (`name`) VALUES( @name )";
            var Actual = Target.SendSync(inputRequest);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        /// <summary>
        /// To ensure that send sync with null request, the appropriate fault message will be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithNullRequest()
        {
            var Target = new DatabaseAdapter();

            var Expected = "Adapter request cannot be null";
            var Actual = Target.SendSync(null);

            Assert.Equal(Expected, Actual.Match<string>(r => string.Empty, e => ((List<Fault>)e).First().Details));
        }

        private Payload CreateFault(string message)
        {
            var faultList = new List<Fault>();
            var exception = new OrbitalAdapterException(message);
            var fault = new FaultFactory().CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList);
        }
    }

    /// <summary>
    /// The database exception for test uses.
    /// </summary>
    internal class TestDbException : DbException
    {
        internal TestDbException(string message) : base(message) { }

    }
}
