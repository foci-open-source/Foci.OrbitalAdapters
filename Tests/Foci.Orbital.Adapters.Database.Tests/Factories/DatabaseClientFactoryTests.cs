﻿using Foci.Orbital.Adapters.Database.DatabaseClients;
using Foci.Orbital.Adapters.Database.DatabaseClients.Interfaces;
using Foci.Orbital.Adapters.Database.DBClients;
using Foci.Orbital.Adapters.Database.Factories;
using Foci.Orbital.Adapters.Database.Models;
using Xunit;

namespace Foci.Orbital.Adapters.Database.Tests.Factories
{
    public class DatabaseClientFactoryTests
    {
        /// <summary>
        /// To ensure that get Mysql client with DatabaseType.MYSQL will get the expected mysql client.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetMySQLClientSuccess()
        {
            var Input = new
            {
                databaseType = DatabaseType.MYSQL,
                connectionString = "Server=localhost;Database=Customer;Uid=root;Pwd=example"
            };

            var factory = new DatabaseClientFactory();

            var Acutal = factory.getClient(Input.databaseType, Input.connectionString);

            Assert.IsType<MySqlClient>(Acutal);
        }

        /// <summary>
        /// To ensure that get SQLite client with DatabaseType.SQLite will get the expected SQLite client.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetSQLiteClientSuccess()
        {
            var Input = new
            {
                databaseType = DatabaseType.SQLITE,
                connectionString = "Data Source=test.db"
            };

            var factory = new DatabaseClientFactory();

            var Acutal = factory.getClient(Input.databaseType, Input.connectionString);

            Assert.IsType<SQLiteClient>(Acutal);
        }

        /// <summary>
        /// To ensure that get PostgreSQL client with DatabaseType.POSTGRESQL will get the expected mysql client.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetPostgreSQLClientSuccess()
        {
            var Input = new
            {
                databaseType = DatabaseType.POSTGRESQL,
                connectionString = "Server=127.0.0.1;Port=5432;Database=myDataBase;Userid=myUsername;Password=myPassword;"
            };

            var factory = new DatabaseClientFactory();

            var Acutal = factory.getClient(Input.databaseType, Input.connectionString);

            Assert.IsType<PostgreSQLClient>(Acutal);
        }

        /// <summary>
        /// To ensure that get MSSQL client with DatabaseType.MSSQL will get the expected MSSQL client.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetMSSQLClientSuccess()
        {
            var Input = new
            {
                databaseType = DatabaseType.MSSQL,
                connectionString = "Server=myServerName;Database=myDataBase;User Id=myUsername;Password = myPassword;"
            };

            var factory = new DatabaseClientFactory();

            var Acutal = factory.getClient(Input.databaseType, Input.connectionString);

            Assert.IsType<MSSQLClient>(Acutal);
        }

        /// <summary>
        /// To ensure that get Oracle client with DatabaseType.MSSQL will get the expected MSSQL client.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetORACLEClientSuccess()
        {
            var Input = new
            {
                databaseType = DatabaseType.ORACLE,
                connectionString = "User Id=hr;Password=hr;Data Source=localhost:1521/orcl;"
            };

            var factory = new DatabaseClientFactory();

            var Acutal = factory.getClient(Input.databaseType, Input.connectionString);

            Assert.IsType<OracleClient>(Acutal);
        }

        /// <summary>
        /// To ensure that if the database type is undefined, a null would be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetClientWithUndefinedDatabaseType()
        {
            var Input = new
            {
                databaseType = DatabaseType.UNDEFINED,
                connectionString = "Server=localhost;Database=Customer;Uid=root;Pwd=example"
            };

            var factory = new DatabaseClientFactory();

            var Acutal = factory.getClient(Input.databaseType, Input.connectionString);

            Assert.Null(Acutal);
        }
    }
}
