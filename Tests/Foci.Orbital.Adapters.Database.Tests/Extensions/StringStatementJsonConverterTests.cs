﻿using Bogus;
using Foci.Orbital.Adapters.Database.Extensions;
using Foci.Orbital.Adapters.Database.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Database.Tests.Extensions
{
    [ExcludeFromCodeCoverage]
    public class StringStatementJsonConverterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Expect the converter to be able to read and not able to write
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanReadIsTrue()
        {
            var Target = new StringStatementJsonConverter();

            Assert.True(Target.CanRead);
            Assert.False(Target.CanWrite);
        }

        /// <summary>
        /// Testing CanConvert with correct type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanConvertWithValidType()
        {
            var Target = new StringStatementJsonConverter();

            var input = typeof(string);

            Assert.True(Target.CanConvert(input));
        }


        /// <summary>
        /// Testing CanConvert with invalid type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void CanConvertWithInvalidType()
        {
            var Target = new StringStatementJsonConverter();

            var input = typeof(int);

            Assert.False(Target.CanConvert(input));
        }

        /// <summary>
        /// Test deserializing QUERY to proper accepted statement enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithQUERYString()
        {
            var Input = "{\"Result\":\"QUERY\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = Statement.QUERY;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing EXECUTE to proper accepted statement enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithEXECUTEString()
        {
            var Input = "{\"Result\":\"EXECUTE\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = Statement.EXECUTE;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing string to proper accepted statement enum
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonWithNullReader()
        {
            var Target = new StringStatementJsonConverter();

            var Actual = Target.ReadJson(JObject.Parse("{}").CreateReader(), null, null, null);
            var Expected = Statement.UNDEFINED;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test deserializing invalid string to Statement.UNDEFINED
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonWithInvalidInput()
        {
            var Input = "{\"Result\":\"" + faker.Random.String() + "\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = Statement.UNDEFINED;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// WriteJson should throw NotImplementedException
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WriteJsonShoudlBeUndefined()
        {
            var Target = new StringStatementJsonConverter();
            Assert.Throws<NotImplementedException>(() => Target.WriteJson(null, null, null));
        }
    }

    /// <summary>
    /// Testing class used to test StringOperatorJsonConverter
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Test
    {
        [JsonConverter(typeof(StringStatementJsonConverter))]
        public Statement Result { get; set; }
    }
}
