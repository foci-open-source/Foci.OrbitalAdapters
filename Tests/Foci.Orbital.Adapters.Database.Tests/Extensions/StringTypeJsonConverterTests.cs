﻿using Bogus;
using Foci.Orbital.Adapters.Database.Extensions;
using Foci.Orbital.Adapters.Database.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Database.Tests.Extensions
{
    [ExcludeFromCodeCoverage]
    public class StringTypeJsonConverterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Expect the converter to be able to read and not able to write
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanReadIsTrue()
        {
            var Target = new StringTypeJsonConverter();

            Assert.True(Target.CanRead);
            Assert.False(Target.CanWrite);
        }

        /// <summary>
        /// Testing CanConvert with correct type
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanConvertWithValidType()
        {
            var Target = new StringTypeJsonConverter();

            var input = typeof(string);

            Assert.True(Target.CanConvert(input));
        }

        /// <summary>
        /// Testing CanConvert with invalid type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void CanConvertWithInvalidType()
        {
            var Target = new StringTypeJsonConverter();

            var input = typeof(int);

            Assert.False(Target.CanConvert(input));
        }

        /// <summary>
        /// Test deserializing MYSQL to proper accepted database type enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithMYSQLString()
        {
            var Input = "{\"Result\":\"MYSQL\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = DatabaseType.MYSQL;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing SQLite to proper accepted database type enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithSQLITEString()
        {
            var Input = "{\"Result\":\"SQLITE\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = DatabaseType.SQLITE;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing MSSQL to proper accepted database type enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithMSSQLString()
        {
            var Input = "{\"Result\":\"MSSQL\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = DatabaseType.MSSQL;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing postgreSQL to proper accepted database type enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithPOSTGRESQLString()
        {
            var Input = "{\"Result\":\"POSTGRESQL\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = DatabaseType.POSTGRESQL;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing Oracle to proper accepted database type enum
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWithORACLEString()
        {
            var Input = "{\"Result\":\"ORACLE\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = DatabaseType.ORACLE;

            Assert.Equal(Expected, Actual.Result);
        }

        /// <summary>
        /// Test deserializing string to proper accepted database type enum
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonWithNullReader()
        {
            var Target = new StringTypeJsonConverter();

            var Actual = Target.ReadJson(JObject.Parse("{}").CreateReader(), null, null, null);
            var Expected = DatabaseType.UNDEFINED;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test deserializing invalid string to DatabaseType.UNDEFINED
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonWithInvalidInput()
        {
            var Input = "{\"Result\":\"" + faker.Random.String() + "\"}";

            var Actual = JsonConvert.DeserializeObject<Test>(Input);
            var Expected = DatabaseType.UNDEFINED;

            Assert.Equal(Expected, Actual.Result);
        }


        /// <summary>
        /// WriteJson should throw NotImplementedException
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void WriteJsonShoudlBeUndefined()
        {
            var Target = new StringTypeJsonConverter();
            Assert.Throws<NotImplementedException>(() => Target.WriteJson(null, null, null));
        }

        /// <summary>
        /// Testing class used to test StringOperatorJsonConverter
        /// </summary>
        [ExcludeFromCodeCoverage]
        public class Test
        {
            [JsonConverter(typeof(StringTypeJsonConverter))]
            public DatabaseType Result { get; set; }
        }
    }
}
