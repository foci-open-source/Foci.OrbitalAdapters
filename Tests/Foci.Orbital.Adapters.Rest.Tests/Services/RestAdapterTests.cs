﻿using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Rest.Factories.Interfaces;
using Foci.Orbital.Adapters.Rest.Models;
using Foci.Orbital.Adapters.Rest.Services;
using Newtonsoft.Json;
using NSubstitute;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using Xunit;

namespace Foci.Orbital.Adapters.Rest.Tests.Services
{
    /// <summary>
    /// Testing the five of the six contracts of IRestAdapterService in RestAdapter. The two parameter Initialize is implicitly tested
    /// within the other contracts.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestAdapterTests
    {
        private readonly Faker faker = new Faker();

        #region Security OFF
        /// <summary>
        /// Success route of sending an object synchronously without a body and receive an object of specific type back.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncronouslySuccess()
        {
            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer() { Address = new Address() { AddressLine1 = "Arbitrary Address" }, Name = new Name() { FirstName = "Arbitrary Name" }, Id = 666 }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110"
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.Content.Returns(Input.TestCustomer);
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ContentType.Returns(Input.ContentType);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header> { new Header() { Key = "Content-Type", Value = "application/json" } }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Method.GET).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);
            #endregion


            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };

            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetCustomerConfiguration.json"), string.Empty, readonlyparams);

            var Actual = Target.SendSync(syncAdapterSend);
            var Expected = Input.TestCustomer;

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        // <summary>
        /// Check for proper exception being thrown when the service configuration loads to a null object.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronouslyNullConfigFailure()
        {
            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer() { Address = new Address() { AddressLine1 = "Arbitrary Address" }, Name = new Name() { FirstName = "Arbitrary Name" }, Id = 666 }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110",
            };

            #region Substitutes
            var RestClientFactory = Substitute.For<IRestClientFactory>();

            #endregion

            var emptydictionary = new Dictionary<string, string>();
            var readonlyemptydictionary = new ReadOnlyDictionary<string, string>(emptydictionary);

            var Target = new RestAdapter(RestClientFactory);
            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/EmptyConfig.json"), string.Empty, readonlyemptydictionary);

            Assert.NotNull(Target.SendSync(syncAdapterSend));
        }

        /// <summary>
        /// Check for proper exception being thrown when the service configuration loads to a null object.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncWithBodyNullConfigFailure()
        {


            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer() { Address = new Address() { AddressLine1 = "Arbitrary Address" }, Name = new Name() { FirstName = "Arbitrary Name" }, Id = 666 }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110",
            };
            var emptydictionary = new Dictionary<string, string>();
            var readonlyemptydictionary = new ReadOnlyDictionary<string, string>(emptydictionary);

            #region Substitutes
            var RestClientFactory = Substitute.For<IRestClientFactory>();

            #endregion


            var Target = new RestAdapter(RestClientFactory);
            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/EmptyConfig.json"), Input.TestCustomer, readonlyemptydictionary);
            Assert.NotNull(Target.SendSync(syncAdapterSend));
        }

        /// <summary>
        /// Failure route of sending an object synchronously without a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronouslyFailure()
        {
            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer()
                {
                    Address = new Address() { AddressLine1 = faker.Random.String() },
                    Name = new Name() { FirstName = faker.Random.String() },
                    Id = faker.Random.Int()
                }
                ),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110"
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.NotFound);
            RestResponse.ContentType.Returns(Input.ContentType);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Error);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header> { new Header() { Key = "Content-Type", Value = "application/json" } }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Method.GET).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);
            #endregion

            var Expected = Fault.Create("Https status code: NotFound Https status code description: ", OrbitalFaultCode.Orbital_Business_Adapter_Error_012);

            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetCustomerConfiguration.json"), string.Empty, readonlyparams);

            var Actual = Target.SendSync(syncAdapterSend);

            Assert.Equal(Actual.Match<IEnumerable<Fault>>(x => null, x => x).ToList().First().OrbitalFaultCode, Expected.OrbitalFaultCode);
            Assert.Equal(Actual.Match<IEnumerable<Fault>>(x => null, x => x).ToList().First().OrbitalFaultName, Expected.OrbitalFaultName);
            Assert.Equal(Actual.Match<IEnumerable<Fault>>(x => null, x => x).ToList().First().ErrorDescription, Expected.ErrorDescription);

        }

        /// <summary>
        /// Failure route of sending an object synchronously without a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSyncronouslyXmlFailure()
        {
            var Input = new
            {
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110"
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ContentType.Returns("application/xml");
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header> { new Header() { Key = "Content-Type", Value = "application/json" } }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Method.GET).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);


            #endregion

            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetCustomerConfiguration.json"), string.Empty, readonlyparams);
            Assert.NotNull(Target.SendSync(syncAdapterSend));
        }


        /// <summary>
        /// Success route of sending an object synchronously WITH a body and receive an object of specific type back.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSyncronouslyWithGenericObjectSuccess()
        {

            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer()),
                Ack = JsonConvert.SerializeObject(new CustomerAck() { Ack = true }),
                ContentType = "application/json",
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer?Id=110",
                Method = Method.GET
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.Content.Returns(Input.Ack);
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ContentType.Returns(Input.ContentType);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute(Input.TestCustomer).Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);


            #endregion


            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSendWithBody = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetCustomerConfiguration.json"), Input.TestCustomer, readonlyparams);


            var Actual = Target.SendSync(syncAdapterSendWithBody);
            var Expected = Input.Ack;

            RestClientFactory.Received().CreateRestClient(Input.Uri);
            RestActionFactory.Received().CreateRequest(null, Input.Method);
            RestRequestFactory.ReceivedWithAnyArgs().AddHeaders(new List<Header>() { Input.Header });
            RestRequestFactory.Received().Execute(Input.TestCustomer);

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        /// <summary>
        /// Success route of asynchronously sending a request without a body or parameters.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendAsyncWithoutParamsSuccess()
        {

            var Input = new
            {
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer",
                Method = Method.GET
            };

            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);


            #endregion

            var Target = new RestAdapter(RestClientFactory);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/SimpleGetConfiguration.json"), string.Empty, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestRequestFactory.Received().Execute();

        }

        /// <summary>
        /// Failure to create secure client if we miss one of the field.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void MissingAuthenicationField()
        {
            var Input = new
            {
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer",
                Method = Method.GET
            };
            #region Substitutions
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.BadRequest);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);
            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);
            #endregion
            var Target = new RestAdapter(RestClientFactory);

            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/EmptyPasswordConfig.json"), string.Empty, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestClientFactory.Received().CreateRestClient(Input.Uri);
            RestActionFactory.Received().CreateRequest(null, Input.Method);
            RestRequestFactory.ReceivedWithAnyArgs().AddHeaders(new List<Header>() { Input.Header });
        }

        /// <summary>
        /// Failure route if we get an error exception response from the RestClient.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ErrorExceptionResponse()
        {
            var Input = new
            {
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer",
                Method = Method.GET,
                Exception = new Exception()
            };
            #region Substitutions
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.ErrorException.Returns(Input.Exception);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);
            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);
            #endregion

            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var Target = new RestAdapter(RestClientFactory);
            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/SimpleGetConfiguration.json"), string.Empty, readonlyparams);

            var exception = new OrbitalAdapterException("There was a network issue while trying to send the message", Input.Exception);
            var fault = new FaultFactory().CreateFault(exception);
            var faultList = new List<Fault>() { fault };
            var Expected = Payload.Create(faultList);

            var Actual = Target.SendSync(asynAdapterMessage);

            Assert.True(PayloadFaultEqual(Actual, Expected));
        }


        /// <summary>
        /// Success route of asynchronously sending a request without a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendAsyncSuccess()
        {

            var Input = new
            {
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer?Id=110",
                Method = Method.GET
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);


            #endregion

            var Target = new RestAdapter(RestClientFactory);

            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetCustomerConfiguration.json"), string.Empty, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestRequestFactory.Received().Execute();

        }

        /// <summary>
        /// Success route of asynchronously sending a request with a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendAsyncWithMessageSuccess()
        {
            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer()),
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer?Id=110",
                Method = Method.GET
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute(Input.TestCustomer).Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateRestClient(Input.Uri).Returns(RestActionFactory);


            #endregion

            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetCustomerConfiguration.json"), Input.TestCustomer, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestRequestFactory.Received().Execute(Input.TestCustomer);

        }

        #endregion

        #region Security ON

        /// <summary>
        /// Success route of sending an object synchronously without a body and receive an object of specific type back.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSecureSyncronouslySuccess()
        {


            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer() { Address = new Address() { AddressLine1 = "Arbitrary Address" }, Name = new Name() { FirstName = "Arbitrary Name" }, Id = 666 }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110"
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.Content.Returns(Input.TestCustomer);
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ContentType.Returns(Input.ContentType);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);



            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header> { new Header() { Key = "Content-Type", Value = "application/json" } }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Method.GET).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateSecureRestClient(Input.Uri, "username", "password").Returns(RestActionFactory);
            #endregion


            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetSecureCustomerConfiguration.json"), string.Empty, readonlyparams);

            var Actual = Target.SendSync(syncAdapterSend);
            var Expected = Input.TestCustomer;

            Assert.Equal(Expected, Actual.Match(x => x, x => null));
        }

        // <summary>
        /// Check for proper exception being thrown when the service configuration loads to a null object.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSecureSyncronouslyNullConfigFailure()
        {


            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer() { Address = new Address() { AddressLine1 = "Arbitrary Address" }, Name = new Name() { FirstName = "Arbitrary Name" }, Id = 666 }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110",
            };

            #region Substitutes
            var RestClientFactory = Substitute.For<IRestClientFactory>();

            #endregion

            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var Target = new RestAdapter(RestClientFactory);
            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/EmptyConfig.json"), string.Empty, readonlyparams);

            Assert.NotNull(Target.SendSync(syncAdapterSend));
        }

        /// <summary>
        /// Check for proper exception being thrown when the service configuration loads to a null object.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSecureSyncWithBodyNullConfigFailure()
        {


            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer() { Address = new Address() { AddressLine1 = "Arbitrary Address" }, Name = new Name() { FirstName = "Arbitrary Name" }, Id = 666 }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110",
            };

            #region Substitutes
            var RestClientFactory = Substitute.For<IRestClientFactory>();

            #endregion

            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var Target = new RestAdapter(RestClientFactory);
            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/EmptyConfig.json"), Input.TestCustomer, readonlyparams);

            Assert.NotNull(Target.SendSync(syncAdapterSend));
        }

        /// <summary>
        /// Failure route of sending an object synchronously without a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSecureSyncronouslyFailure()
        {


            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer()
                {
                    Address = new Address() { AddressLine1 = faker.Random.String() },
                    Name = new Name() { FirstName = faker.Random.String() },
                    Id = faker.Random.Int()
                }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110"
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.NotFound);
            RestResponse.ContentType.Returns(Input.ContentType);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Error);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header> { new Header() { Key = "Content-Type", Value = "application/json" } }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Method.GET).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateSecureRestClient(Input.Uri, "username", "password").Returns(RestActionFactory);
            #endregion

            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetSecureCustomerConfiguration.json"), string.Empty, readonlyparams);

            var Actual = Target.SendSync(syncAdapterSend);

            Assert.NotNull(Actual);
        }

        /// <summary>
        /// Failure route of sending an object synchronously without a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SendSecureSyncronouslyXmlFailure()
        {


            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer()
                {
                    Address = new Address() { AddressLine1 = faker.Random.String() },
                    Name = new Name() { FirstName = faker.Random.String() },
                    Id = faker.Random.Int()
                }),
                ContentType = "application/json",
                Uri = "http://localhost:3580/customer?Id=110"
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ContentType.Returns("application/xml");
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header> { new Header() { Key = "Content-Type", Value = "application/json" } }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Method.GET).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateSecureRestClient(Input.Uri, "username", "password").Returns(RestActionFactory);
            #endregion

            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var syncAdapterSend = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetSecureCustomerConfiguration.json"), string.Empty, readonlyparams);
            Assert.NotNull(Target.SendSync(syncAdapterSend));
        }



        /// <summary>
        /// Success route of asynchronously sending a request without a body or parameters.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSecureAsyncWithoutParamsSuccess()
        {
            var Input = new
            {
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer",
                Method = Method.GET
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateSecureRestClient(Input.Uri, "username", "password").Returns(RestActionFactory);
            #endregion

            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var Target = new RestAdapter(RestClientFactory);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/SimpleGetSecureConfiguration.json"), string.Empty, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestRequestFactory.Received().Execute();

        }

        /// <summary>
        /// Success route of asynchronously sending a request without a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSecureAsyncSuccess()
        {


            var Input = new
            {
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer?Id=110",
                Method = Method.GET
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute().Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateSecureRestClient(Input.Uri, "username", "password").Returns(RestActionFactory);
            #endregion

            var Target = new RestAdapter(RestClientFactory);

            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetSecureCustomerConfiguration.json"), string.Empty, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestRequestFactory.Received().Execute();

        }

        /// <summary>
        /// Success route of asynchronously sending a request with a body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SendSecureAsyncWithMessageSuccess()
        {
            var Input = new
            {
                TestCustomer = JsonConvert.SerializeObject(new Customer()),
                Header = new Header() { Key = "Content-Type", Value = "application/json" },
                Uri = "http://localhost:3580/customer?Id=110",
                Method = Method.GET
            };

            #region Substitutes
            var RestResponse = Substitute.For<IRestResponse>();
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);

            var RestRequestFactory = Substitute.For<IRestRequestFactory>();
            RestRequestFactory.AddHeaders(new List<Header>() { Input.Header }).ReturnsForAnyArgs(RestRequestFactory);
            RestRequestFactory.Execute(Input.TestCustomer).Returns(RestResponse);

            var RestActionFactory = Substitute.For<IRestActionFactory>();
            RestActionFactory.CreateRequest(null, Input.Method).Returns(RestRequestFactory);

            var RestClientFactory = Substitute.For<IRestClientFactory>();
            RestClientFactory.CreateSecureRestClient(Input.Uri, "username", "password").Returns(RestActionFactory);

            #endregion

            var Target = new RestAdapter(RestClientFactory);
            var @params = new Dictionary<string, string>
            {
                { "customerId", "110" }
            };
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);

            var asynAdapterMessage = new AdapterRequest(File.ReadAllText("./ServiceConfigurations/GetSecureCustomerConfiguration.json"), Input.TestCustomer, readonlyparams);

            Target.SendAsync(asynAdapterMessage);

            RestRequestFactory.Received().Execute(Input.TestCustomer);
        }
        #endregion

        /// <summary>
        /// Helper utility to help test the equality of payloads.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="faultEquality"></param>
        /// <returns></returns>
        private static bool PayloadFaultEqual(Payload a, Payload b)
        {
            string aleft = default(string);
            IEnumerable<Fault> afault = default(IEnumerable<Fault>);

            string bleft = default(string);
            IEnumerable<Fault> bfault = default(IEnumerable<Fault>);

            a.Match(
                value => { aleft = value; return true; },
                fault => { afault = fault; return true; }
            );

            b.Match(
                value => { bleft = value; return true; },
                fault => { bfault = fault; return true; }
            );

            if (afault != default(IEnumerable<Fault>) && bfault != default(IEnumerable<Fault>))
            {
                return bfault.ToList().First().ErrorDescription.Equals(afault.ToList().First().ErrorDescription)
                    && bfault.ToList().First().OrbitalFaultCode.Equals(afault.ToList().First().OrbitalFaultCode)
                    && bfault.ToList().First().OrbitalFaultName.Equals(afault.ToList().First().OrbitalFaultName);
            }

            return false;
        }
    }

    #region Test Classes
    [ExcludeFromCodeCoverage]
    public class CustomerAck
    {
        public bool Ack { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class Name
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class Address
    {
        public int StreetNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string ProvinceState { get; set; }
        public string Country { get; set; }
        public string PostalZipCode { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class Customer
    {
        public int Id { get; set; }
        public Name Name { get; set; }
        public Address Address { get; set; }
    }


    #endregion
}