﻿using Bogus;
using Foci.Orbital.Adapters.Rest.Factories;
using Foci.Orbital.Adapters.Rest.Models;
using Newtonsoft.Json;
using NSubstitute;
using RestSharp;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using Xunit;

namespace Foci.Orbital.Adapters.Rest.Tests.Factories
{
    /// <summary>
    /// Test RestRequestFactory class with methods that create RestSharper requests.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestRequestFactoryTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Add a parameter to the request success route.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddParameterSuccess()
        {
            #region Substitutions
            var RestClient = Substitute.For<IRestClient>();
            var RestRequest = Substitute.For<IRestRequest>();
            #endregion

            #region Setup
            var Input = new
            {
                ParamName = faker.Random.String(),
                ParamValue = faker.Random.String(),
                ParamType = faker.Random.Enum<ParameterType>()
            }; 
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);
            Target.AddParameter(Input.ParamName, Input.ParamValue, Input.ParamType);

            RestRequest.Received().AddParameter(Input.ParamName, Input.ParamValue, Input.ParamType);
        }

        /// <summary>
        /// Add a single header to the request success route.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddHeaderSuccess()
        {
            #region Substitutions
            var RestClient = Substitute.For<IRestClient>();
            var RestRequest = Substitute.For<IRestRequest>();
            #endregion

            #region Setup
            var Input = new
            {
                HeaderName = faker.Random.String(),
                HeaderValue = faker.Random.String()
            };
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);
            Target.AddHeader(Input.HeaderName, Input.HeaderValue);

            RestRequest.Received().AddHeader(Input.HeaderName, Input.HeaderValue);
        }

        /// <summary>
        /// Add a list of headers to the request success route.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddHeadersSuccess()
        {
            #region Substitutions
            var RestClient = Substitute.For<IRestClient>();
            var RestRequest = Substitute.For<IRestRequest>();
            #endregion

            #region Setup
            var Input = new
            {
                headerKey1 = faker.Random.String(),
                headerKey2 = faker.Random.String(),
                headerValue = faker.Random.String()
            };
            var inputHeaders = new List<Header>()
                {
                    new Header() {Key = Input.headerKey1, Value = Input.headerValue},
                    new Header() {Key = Input.headerKey2, Value = Input.headerValue}
                };
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);
            Target.AddHeaders(inputHeaders);

            RestRequest.Received().AddHeader(Input.headerKey1, Input.headerValue);
            RestRequest.Received().AddHeader(Input.headerKey2, Input.headerValue);
        }

        /// <summary>
        /// Success route of client execution with no arguments.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunExecuteNoArgumentsSuccess()
        {
            #region Substitutions
            var RestResponse = Substitute.For<IRestResponse>();
            var RestRequest = Substitute.For<IRestRequest>();
            var RestClient = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            RestClient.Execute(RestRequest).Returns(RestResponse);
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);

            var Actual = Target.Execute();
            var Expected = RestResponse;

            Assert.Equal(Expected, Actual);
            RestClient.Received().Execute(RestRequest);
        }

        /// <summary>
        /// Success route of client execution with json body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunExecuteWithJsonBodySuccess()
        {
            #region Substitutions
            var RestResponse = Substitute.For<IRestResponse>();
            var RestRequest = Substitute.For<IRestRequest>();
            var RestClient = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                Body = faker.Random.String(),
                Key = "Content-Type",
                Value = "application/json"
            };
            RestClient.Execute(RestRequest).Returns(RestResponse);
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);
            Target.AddHeader(Input.Key, Input.Value);

            var Actual = Target.Execute(Input.Body);
            var Expected = RestResponse;

            Assert.Equal(Expected, Actual);
            RestRequest.Received().AddParameter(Input.Value, Input.Body, ParameterType.RequestBody);
        }

        /// <summary>
        /// ) route of client type execution with no body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunExecuteWithTypeSuccess()
        {
            #region Substitutions
            var RestResponse = Substitute.For<IRestResponse>();
            var RestRequest = Substitute.For<IRestRequest>();
            var RestClient = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                Data = new TestClassString() { TestString = faker.Random.String() },
                Key = "Content-Type",
                Value = "application/json"
            };
            RestResponse.ContentType.Returns(Input.Value);
            RestResponse.Content.Returns(JsonConvert.SerializeObject(Input.Data));
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);
            RestClient.Execute(RestRequest).Returns(RestResponse); 
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);
            Target.AddHeader(Input.Key, Input.Value);

            var Actual = Target.Execute();
            var Expected = RestResponse;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Success route of client type execution with body.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RunExecuteWithTypeAndBodySuccess()
        {
            #region Substitutions
            var RestResponse = Substitute.For<IRestResponse>();
            var RestRequest = Substitute.For<IRestRequest>();
            var RestClient = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                Data = JsonConvert.SerializeObject(new TestClassString() { TestString = faker.Random.String() }),
                Key = "Content-Type",
                Value = "application/json"
            };
            RestResponse.ContentType.Returns(Input.Value);
            RestResponse.Content.Returns(JsonConvert.SerializeObject(Input.Data));
            RestResponse.StatusCode.Returns(HttpStatusCode.OK);
            RestResponse.ResponseStatus.Returns(ResponseStatus.Completed);

            RestClient.Execute(RestRequest).Returns(RestResponse);
            #endregion

            var Target = new RestRequestFactory(RestClient, RestRequest);
            Target.AddHeader(Input.Key, Input.Value);

            var Actual = Target.Execute(Input.Data);
            var Expected = RestResponse;

            Assert.Equal(Expected, Actual);
            RestRequest.Received().AddParameter(Input.Value, Input.Data, ParameterType.RequestBody);
        }

        /// <summary>
        /// Small class containing a string property for testing purposes.
        /// </summary>
        public class TestClassString
        {
            public string TestString { get; set; }
        }
    }
}
