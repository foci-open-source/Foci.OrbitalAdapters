﻿using Bogus;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Rest.Tests.Extensions
{
    /// <summary>
    /// Tests to compare customized serializers with default serializers.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class JsonSerializerTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Check for success in returning properties by reflection.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PropertiesTests()
        {
            var Target = new Faker<Rest.Extensions.JsonSerializer>()
                .RuleFor(j=>j.DateFormat, f=>f.Random.String())
                .RuleFor(j => j.RootElement, f => f.Random.String())
                .RuleFor(j => j.Namespace, f => f.Random.String())
                .Generate();

            foreach (var prop in Target.GetType().GetProperties())
            {
                Assert.NotNull(prop.GetValue(Target));
            }
        }

        /// <summary>
        /// Check if default serialization matches NewtonSoft Json default serialization.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CustomSerializerDefaultSerializer()
        {
            var Target = new Rest.Extensions.JsonSerializer();

            var Input = new
            {
                TestObject = new TestClass() {
                    TestString = faker.Random.String()
                }
            };

            var Actual = Target.Serialize(Input.TestObject).Replace("\n", string.Empty).Replace(" ", string.Empty).Replace("\r", string.Empty);
            var Expected = JsonConvert.SerializeObject(Input.TestObject);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Check if customized serialization matches NewtonSoft Json default serialization.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CustomSerializerWithJsonSerializer()
        {
            var Input = new
            {
                TestObject = new TestClass() {
                    TestString = faker.Random.String()
                },
                JsonSerializer = new JsonSerializer()
            };

            var Target = new Rest.Extensions.JsonSerializer(Input.JsonSerializer);

            var Actual = Target.Serialize(Input.TestObject).Replace("\n", string.Empty).Replace(" ", string.Empty).Replace("\r", string.Empty);
            var Expected = JsonConvert.SerializeObject(Input.TestObject);

            Assert.Equal(Expected, Actual);
        }
    }

    /// <summary>
    /// A test class for this test case.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class TestClass
    {
        public string TestString { get; set; }
    }
}
