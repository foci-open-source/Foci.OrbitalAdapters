﻿using Bogus;
using Foci.Orbital.Adapters.Rest.Extensions;
using RestSharp;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using Xunit;

namespace Foci.Orbital.Adapters.Rest.Tests.Extensions
{
    /// <summary>
    /// Tests to check the data in a RestResponse is interpreted correctly. 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestSharpTests
    {
        /// <summary>
        /// Compares that the values found in the Dictionary match with reflection property values.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToDictionaryTests()
        {
            #region Setup
            var Input = new
            {

                RestResponse = new Faker<RestResponse>()
                .RuleFor(r => r.Request, new RestRequest())
                .RuleFor(r => r.ContentType, f => f.Random.String())
                .RuleFor(r => r.ContentLength, f => f.Random.Long())
                .RuleFor(r => r.ContentEncoding, f => f.Random.String())
                .RuleFor(r => r.StatusCode, f => f.Random.Enum<HttpStatusCode>())
                .RuleFor(r => r.StatusDescription, f => f.Random.String())
                .RuleFor(r => r.RawBytes, f => f.Random.Bytes(10))
                .RuleFor(r => r.ResponseUri, f => new Uri(f.Internet.Url()))
                .RuleFor(r => r.Server, f => f.Random.String())
                .RuleFor(r => r.ErrorMessage, f => f.Random.String())
                .Generate(),
            }; 
            #endregion

            var Target = Input.RestResponse.ToDictionary();

            foreach (var property in Input.RestResponse.GetType().GetProperties().Where(prop => prop.Name != "Content"))
            {
                var value = property.GetValue(Input.RestResponse);
                var Expected = value == null ? "null" : value.ToString();
                var Actual = Target[property.Name];
                Assert.Equal(Expected, Actual);
            }
        }

        /// <summary>
        /// Check for the correct flag return if a response is marked as successful or failure.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void IsSuccessfulTest()
        {
            #region Setup
            var Input = new
            {
                RestResponseOkCompleted = new Faker<RestResponse>()
                    .RuleFor(r => r.StatusCode, HttpStatusCode.Accepted)
                    .RuleFor(r => r.ResponseStatus, ResponseStatus.Completed)
                    .Generate(),

                RestResponseNotOkCompleted = new Faker<RestResponse>()
                    .RuleFor(r => r.StatusCode, HttpStatusCode.NotFound)
                    .RuleFor(r => r.ResponseStatus, ResponseStatus.Completed)
                    .Generate(),


                RestResponseOkNotCompleted = new Faker<RestResponse>()
                    .RuleFor(r => r.StatusCode, HttpStatusCode.Accepted)
                    .RuleFor(r => r.ResponseStatus, ResponseStatus.Error)
                    .Generate(),

                RestResponseNotOkNotCompleted = new Faker<RestResponse>()
                    .RuleFor(r => r.StatusCode, HttpStatusCode.NotFound)
                    .RuleFor(r => r.ResponseStatus, ResponseStatus.Error)
                    .Generate()
            }; 
            #endregion

            Assert.True(Input.RestResponseOkCompleted.IsCompleted());
            Assert.False(Input.RestResponseNotOkCompleted.IsCompleted());
            Assert.False(Input.RestResponseOkNotCompleted.IsCompleted());
            Assert.False(Input.RestResponseNotOkNotCompleted.IsCompleted());
        }
    }
}
