﻿using System;

namespace Foci.Orbital.Adapters.Mock.Models
{
    internal struct ComparableValue : IEquatable<ComparableValue>, IComparable<ComparableValue>
    {
        private readonly string input;
        private readonly bool? boolValue;
        private readonly double? doubleValue;
        private readonly string stringValue;

        /// <summary>
        /// The default constructor that takes in a string
        /// </summary>
        /// <param name="input">String that represents either a string, a boolean, or a double</param>
        public ComparableValue(string input)
        {
            this.input = input;
            if (bool.TryParse(input, out var boolOut))
            {
                boolValue = boolOut;
            }
            else
            {
                boolValue = null;
            }

            if (double.TryParse(input, out var doubleOut))
            {
                doubleValue = doubleOut;
            }
            else
            {
                doubleValue = null;
            }

            if (!boolValue.HasValue && !doubleValue.HasValue)
            {
                stringValue = input;
            }
            else
            {
                stringValue = null;
            }
        }

        /// <summary>
        /// Check if the current value is the same type as the given value
        /// </summary>
        /// <param name="value">The value to compare to</param>
        /// <returns>True if both underlying values are of the same type, otherwise returns false</returns>
        public bool IsSameType(ComparableValue value)
        {
            return (boolValue.HasValue && value.boolValue.HasValue)
                || (doubleValue.HasValue && value.doubleValue.HasValue)
                || (stringValue != null && value.stringValue != null);
        }

        /// <inheritdoc />
        public static bool operator <(ComparableValue value1, ComparableValue value2)
        {
            return value1.IsSameType(value2) && (value1.CompareTo(value2) < 0);
        }

        /// <inheritdoc />
        public static bool operator >(ComparableValue value1, ComparableValue value2)
        {
            return value1.IsSameType(value2) && (value1.CompareTo(value2) > 0);
        }

        /// <inheritdoc />
        public static bool operator ==(ComparableValue value1, ComparableValue value2)
        {
            return value1.IsSameType(value2) && value1.Equals(value2);
        }

        /// <inheritdoc />
        public static bool operator !=(ComparableValue value1, ComparableValue value2)
        {
            return value1.IsSameType(value2) && !value1.Equals(value2);
        }

        /// <inheritdoc />
        public static bool operator >=(ComparableValue value1, ComparableValue value2)
        {
            return (value1 == value2) || (value1 > value2);
        }

        /// <inheritdoc />
        public static bool operator <=(ComparableValue value1, ComparableValue value2)
        {
            return (value1 == value2) || (value1 < value2);
        }

        /// <inheritdoc />
        public int CompareTo(ComparableValue other)
        {
            if (boolValue.HasValue && other.boolValue.HasValue)
            {
                return boolValue.Value.CompareTo(other.boolValue.Value);
            }
            if (doubleValue.HasValue && other.doubleValue.HasValue)
            {
                return doubleValue.Value.CompareTo(other.doubleValue.Value);
            }

            return input.CompareTo(other.input);
        }

        /// <inheritdoc />
        public bool Equals(ComparableValue other)
        {
            return (boolValue == other.boolValue) &&
                   (doubleValue == other.doubleValue) &&
                   (input == other.input);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
