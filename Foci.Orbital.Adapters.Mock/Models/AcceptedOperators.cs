﻿namespace Foci.Orbital.Adapters.Mock.Models
{
    /// <summary>
    /// Enum containing all valid relational operators for the mock adapter
    /// </summary>
    public enum AcceptedOperators
    {
        UNDEFINED = 0,
        EQ,
        NE,
        GT,
        LT,
        GE,
        LE
    }
}
