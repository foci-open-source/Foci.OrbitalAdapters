﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Mock.Models
{
    /// <summary>
    /// Properties taken from a configuration file for Mock Adapter processes.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class MockAdapterConfiguration
    {
        /// <summary>
        /// String representation of JSONPath expression to obtain the value to compare to the conditions
        /// </summary>
        [JsonProperty("selector")]
        public string Selector { get; internal set; }

        /// <summary>
        /// The list of conditions used to compare to the selected value
        /// </summary>
        [JsonProperty("conditions")]
        public List<Condition> Conditions { get; internal set; } = new List<Condition>();

        /// <summary>
        /// The default response, if the selected value did not match any condition
        /// </summary>
        [JsonProperty("default")]
        public Default DefaultValue { get; internal set; }
    }
}
