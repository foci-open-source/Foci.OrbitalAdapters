﻿using Foci.Orbital.Adapters.Mock.Extensions;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Mock.Models
{
    /// <summary>
    /// The model class containing property for mock adapter configuration's default value
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class Default
    {
        /// <summary>
        /// If set, this value will be returned as an response
        /// </summary>
        [JsonProperty("response", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ObjectToStringJsonConverter))]
        public string Response { get; internal set; }

        /// <summary>
        /// If set, this value will be packaged as an fault and returned as an response
        /// </summary>
        [JsonProperty("exception", NullValueHandling = NullValueHandling.Ignore)]
        public string Exception { get; internal set; }
    }
}
