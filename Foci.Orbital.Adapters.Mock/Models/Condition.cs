﻿using Foci.Orbital.Adapters.Mock.Extensions;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Mock.Models
{
    /// <summary>
    /// The model class containing property for mock adapter configuration's conditions 
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class Condition
    {
        /// <summary>
        /// The relational operator used to compare selected value with the value in compare to
        /// </summary>
        [JsonProperty("operator")]
        [JsonConverter(typeof(StringOperatorJsonConverter))]
        public AcceptedOperators Operator { get; internal set; }

        /// <summary>
        /// String representation of the value that will be used to compare to the selected value
        /// </summary>
        [JsonProperty("compareTo")]
        public string CompareTo { get; internal set; }

        /// <summary>
        /// If set, this value will be returned as an response
        /// </summary>
        [JsonProperty("response", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ObjectToStringJsonConverter))]
        public string Response { get; internal set; }

        /// <summary>
        /// If set, this value will be packaged as an fault and returned as an response
        /// </summary>
        [JsonProperty("exception", NullValueHandling = NullValueHandling.Ignore)]
        public string Exception { get; internal set; }
    }
}
