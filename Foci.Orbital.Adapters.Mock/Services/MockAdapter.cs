﻿using Foci.Orbital.Adapters.Contract;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Mock.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System.Collections.Generic;

namespace Foci.Orbital.Adapters.Mock.Services
{
    /// <summary>
    /// Adapter for mocking response
    /// </summary>
    public class MockAdapter : BaseAdapter
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        private static readonly FaultFactory faultFactory = new FaultFactory();

        /// <inheritdoc />
        public override void SendAsync(AdapterRequest request)
        {
            GetPayload(request);
        }

        /// <inheritdoc />
        public override Payload SendSync(AdapterRequest request)
        {
            return GetPayload(request);
        }

        /// <summary>
        /// Retrieve response message or the exception message from the configuration and package it in payload
        /// </summary>
        /// <param name="request">The object containing request information</param>
        /// <returns>Payload containing a message or a fault</returns>
        private Payload GetPayload(AdapterRequest request)
        {
            string error;
            if (request == null)
            {
                error = string.Format("Adapter request cannot be null");
                log.Error("Send: {0}", error);
                return CreateFault(error);
            }
            var configuration = LoadParamsToConfig<MockAdapterConfiguration>(request.AdapterConfiguration, request.Parameters);
            if (configuration == null)
            {
                error = string.Format("Unable to read adapter configuration ({0})", request.AdapterConfiguration);
                log.Error("Send: {0}", error);
                return CreateFault(error);
            }

            string selectedValue = GetSelectedValue(request, configuration.Selector);
            if (selectedValue == null)
            {
                log.Debug("Send: Cannot retrieve selected value from request message and parameters, use default value instead.");
                return UseDefault(configuration);
            }

            //Attempt to find condition that matches
            foreach (var condition in configuration.Conditions)
            {
                string exception = condition.Exception;
                string response = condition.Response;
                if (Match(selectedValue, condition.Operator, condition.CompareTo))
                {
                    if (response != null)
                    {
                        log.Info("Send: Successfully send the request message");
                        return Payload.Create(response);
                    }

                    error = exception ?? "One condition matched, but exception message isn't provided";
                    log.Debug("Send: {0}", error);
                    return CreateFault(error);
                }
            }

            return UseDefault(configuration);
        }

        // Use default value declared in the configuration
        private Payload UseDefault(MockAdapterConfiguration configuration)
        {
            string error;
            //Use default value/error
            if (configuration.DefaultValue == null)
            {
                error = "The default value is required because the selected value did not match any condition";
                log.Error("Send: {0}", error);
                return CreateFault(error);
            }

            if (configuration.DefaultValue.Response != null)
            {
                log.Info("Send: Successfully send the request message");
                return Payload.Create(configuration.DefaultValue.Response);
            }

            error = configuration.DefaultValue.Exception ?? "Default exception message isn't provided";
            log.Debug("Send: {0}", error);
            return CreateFault(error);
        }

        /// <summary>
        /// Use the provided relational operator to compare selected value with target value
        /// </summary>
        /// <param name="selectedValue">String representation of value to compare with</param>
        /// <param name="relationalOperator">Enum representation of the relational operator</param>
        /// <param name="targetValue">String representation of the value to compare to</param>
        /// <returns>True if the condition matched, false otherwise</returns>
        private bool Match(string selectedValue, AcceptedOperators relationalOperator, string targetValue)
        {
            if (targetValue == null)
            {
                log.Debug("Match: The compare to property in the configuration cannot be null");
                return false;
            }

            ComparableValue selected = new ComparableValue(selectedValue);
            ComparableValue target = new ComparableValue(targetValue);
            switch (relationalOperator)
            {
                case AcceptedOperators.EQ:
                    return selected == target;
                case AcceptedOperators.GE:
                    return selected >= target;
                case AcceptedOperators.GT:
                    return selected > target;
                case AcceptedOperators.LE:
                    return selected <= target;
                case AcceptedOperators.LT:
                    return selected < target;
                case AcceptedOperators.NE:
                    return selected != target;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Get the selected value from request message and parameter using the provided selector
        /// </summary>
        /// <param name="request">Adapter request containing request message and parameter</param>
        /// <param name="selector">String representation of JSONPath expression</param>
        /// <returns>String representation of the selected value or empty string on error</returns>
        private string GetSelectedValue(AdapterRequest request, string selector)
        {
            if (string.IsNullOrWhiteSpace(selector))
            {
                return null;
            }

            try
            {
                var selectorSource = JObject.Parse(request.Message);
                var paramJson = JObject.Parse(JsonConvert.SerializeObject(request.Parameters));
                selectorSource.Merge(paramJson, new JsonMergeSettings
                {
                    MergeArrayHandling = MergeArrayHandling.Union
                });

                JToken selectedValue = selectorSource.SelectToken(selector);
                if (selectedValue == null)
                {
                    log.Debug("GetSelectedValue: Unable to query value using the provided selector ({0})", selector);
                    return null;
                }
                return selectedValue.ToString();
            }
            catch (JsonReaderException)
            {
                log.Debug("GetSelectedValue: The request messages is not a valid JSON string");
                return null;
            }
        }

        //Create fault payload with the given message as exception message
        private Payload CreateFault(string message)
        {
            var faultList = new List<Fault>();
            var exception = new OrbitalAdapterException(message);
            var fault = faultFactory.CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList);
        }
    }
}
