﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Foci.Orbital.Adapters.Mock.Extensions
{
    public class ObjectToStringJsonConverter : JsonConverter
    {
        /// <summary>
        /// Overridden property that determines whether this converter can write json.
        /// </summary>
        public override bool CanRead { get; } = true;

        /// <summary>
        /// Overridden property that determines whether this convert can read json.
        /// </summary>
        public override bool CanWrite { get; } = false;

        /// <summary>
        /// Can convert anything to string
        /// </summary>
        /// <param name="objectType">The type of given object</param>
        /// <returns>True</returns>
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        /// <summary>
        /// Convert json object to string
        /// </summary>
        /// <param name="reader">JsonReader object containing the json string</param>
        /// <param name="objectType">Type: System.Type. Type of the object. Not used in this implementation.</param>
        /// <param name="existingValue">Type: System.Type. The existing value of object being read. Not used in this implementation.</param>
        /// <param name="serializer">Type: Newtonsoft.Json.JsonSerializer. The calling serializer. Not used in this implementation</param>
        /// <returns>string representing the json object</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            if (token.Type == JTokenType.String)
            {
                return reader.Value.ToString();
            }

            return token.ToString(Formatting.None);
        }

        /// <summary>
        /// Base method is not applicable for this class so in this override method, it is thrown an exception instead.
        /// </summary>
        /// <param name="writer">Type: Newtonsoft.Json.JsonWriter. The JsonWriter to write to. Not used in this implementation.</param>
        /// <param name="value">Type: System.Object. The value. Not used in this implementation.</param>
        /// <param name="serializer">Serializer to/from JSON format. Not used in this implementation.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
