﻿using Foci.Orbital.Adapters.Mock.Models;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace Foci.Orbital.Adapters.Mock.Extensions
{
    /// <summary>
    /// Converter for converting string to AcceptedOperators enum object
    /// </summary>
    public class StringOperatorJsonConverter : JsonConverter
    {
        /// <summary>
        /// Overridden property that determines whether this converter can write json.
        /// </summary>
        public override bool CanRead { get; } = true;

        /// <summary>
        /// Overridden property that determines whether this convert can read json.
        /// </summary>
        public override bool CanWrite { get; } = false;

        /// <summary>
        /// Check if the given object is string
        /// </summary>
        /// <param name="objectType">The type of given object</param>
        /// <returns>True if the object is string, false otherwise</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(string).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Convert json string to accepted operators enum object
        /// </summary>
        /// <param name="reader">JsonReader object containing the json string</param>
        /// <param name="objectType">Type: System.Type. Type of the object. Not used in this implementation.</param>
        /// <param name="existingValue">Type: System.Type. The existing value of object being read. Not used in this implementation.</param>
        /// <param name="serializer">Type: Newtonsoft.Json.JsonSerializer. The calling serializer. Not used in this implementation</param>
        /// <returns>accepted operators enum object created from the given json string</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return AcceptedOperators.UNDEFINED;
            }
            AcceptedOperators result = AcceptedOperators.UNDEFINED;

            string json = Regex.Replace((string)reader.Value, @"\s+", string.Empty);

            switch (json)
            {
                case "==":
                    result = AcceptedOperators.EQ;
                    break;
                case "!=":
                    result = AcceptedOperators.NE;
                    break;
                case ">":
                    result = AcceptedOperators.GT;
                    break;
                case "<":
                    result = AcceptedOperators.LT;
                    break;
                case ">=":
                    result = AcceptedOperators.GE;
                    break;
                case "<=":
                    result = AcceptedOperators.LE;
                    break;
            }
            return result;
        }

        /// <summary>
        /// Base method is not applicable for this class so in this override method, it is thrown an exception instead.
        /// </summary>
        /// <param name="writer">Type: Newtonsoft.Json.JsonWriter. The JsonWriter to write to. Not used in this implementation.</param>
        /// <param name="value">Type: System.Object. The value. Not used in this implementation.</param>
        /// <param name="serializer">Serializer to/from JSON format. Not used in this implementation.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
