# Orbital Adapters

One of the essential components for the [Orbital](http://orbitalbus.com/) are Adapters. The adapters allow the Orbital Agent to communicate to consumers over different protocols, databases, or file systems. Over time more Orbital Adapters will appear for more protocols, databases, and file systems.

For more information about [Orbital](http://orbitalbus.com) check out [http://orbital.com](http://orbitalbus.com)!

## Current Orbital Adapters:

 * REST

 Don’t see an Orbital Adapter of your choice? Check out [how to get involved](http://orbitalbus.com/articles/README.html#how-to-get-involved).

## How Orbital Agent implements an Adapter

The Orbital Agent is responsible for passing all the necessary information to the adapter in order to communicate with a consumer. The adapter is the point of contact from the Orbital to and from the consumer, and it houses the code which will render communication from C# to the consumer's possible protocol.

## Licensing Information

The Orbital Adapters are licensed under the 3-Clause BSD license for open-source software. Please see the [LICENSE](LICENSE.md) file for more information.  

## Contact Information
For inquiries about the project, please e-mail us at [opensource@focisolutions.com](mailto:opensource@focisolutions.com). 