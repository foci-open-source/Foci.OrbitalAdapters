﻿using Foci.Orbital.Adapters.Database.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Foci.Orbital.Adapters.Database.Extensions
{
    /// <summary>
    /// Converter for converting string to statement enum object
    /// </summary>
    public class StringStatementJsonConverter : JsonConverter
    {
        /// <summary>
        /// Overridden property that determines whether this converter can write json.
        /// </summary>
        public override bool CanRead { get; } = true;

        /// <summary>
        /// Overridden property that determines whether this converter can read json.
        /// </summary>
        public override bool CanWrite { get; } = false;

        /// <summary>
        /// Check if the given object is string
        /// </summary>
        /// <param name="objectType">The type of given object</param>
        /// <returns>True if the object is string, false otherwise</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(string).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Convert json string to accepted statement enum object
        /// </summary>
        /// <param name="reader">JsonReader object containing the json string</param>
        /// <param name="objectType">Type: System.Type. Type of the object. Not used in this implementation.</param>
        /// <param name="existingValue">Type: System.Type. The existing value of object being read. Not used in this implementation.</param>
        /// <param name="serializer">Type: Newtonsoft.Json.JsonSerializer. The calling serializer. Not used in this implementation</param>
        /// <returns>accepted operators enum object created from the given json string</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return Statement.UNDEFINED;
            }

            Statement statement = Statement.UNDEFINED;
            string json = Regex.Replace((string)reader.Value, @"\s+", string.Empty);

            switch (json)
            {
                case "QUERY":
                    statement = Statement.QUERY;
                    break;
                case "EXECUTE":
                    statement = Statement.EXECUTE;
                    break;
            }
            return statement;
        }

        /// <summary>
        /// Base method is not applicable for this class so in this override method, it is thrown an exception instead.
        /// </summary>
        /// <param name="writer">Type: Newtonsoft.Json.JsonWriter. The JsonWriter to write to. Not used in this implementation.</param>
        /// <param name="value">Type: System.Object. The value. Not used in this implementation.</param>
        /// <param name="serializer">Serializer to/from JSON format. Not used in this implementation.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
