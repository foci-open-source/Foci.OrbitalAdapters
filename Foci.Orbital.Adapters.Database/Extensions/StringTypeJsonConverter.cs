﻿using Foci.Orbital.Adapters.Database.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Foci.Orbital.Adapters.Database.Extensions
{
    public class StringTypeJsonConverter : JsonConverter
    {
        /// <summary>
        /// Overridden property that determines whether this converter can write json.
        /// </summary>
        public override bool CanRead { get; } = true;

        /// <summary>
        /// Overridden property that determines whether this convert can read json.
        /// </summary>
        public override bool CanWrite { get; } = false;

        /// <summary>
        /// Check if the given object is string
        /// </summary>
        /// <param name="objectType">The type of given object</param>
        /// <returns>True if the object is string, false otherwise</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(string).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Convert json string to accepted database type enum object
        /// </summary>
        /// <param name="reader">JsonReader object containing the json string</param>
        /// <param name="objectType">Type: System.Type. Type of the object. Not used in this implementation.</param>
        /// <param name="existingValue">Type: System.Type. The existing value of object being read. Not used in this implementation.</param>
        /// <param name="serializer">Type: Newtonsoft.Json.JsonSerializer. The calling serializer. Not used in this implementation</param>
        /// <returns>accepted operators enum object created from the given json string</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return DatabaseType.UNDEFINED;
            }

            DatabaseType statement = DatabaseType.UNDEFINED;
            string json = Regex.Replace((string)reader.Value, @"\s+", string.Empty);

            switch (json)
            {
                case "SQLITE":
                    statement = DatabaseType.SQLITE;
                    break;
                case "MYSQL":
                    statement = DatabaseType.MYSQL;
                    break;
                case "MSSQL":
                    statement = DatabaseType.MSSQL;
                    break;
                case "POSTGRESQL":
                    statement = DatabaseType.POSTGRESQL;
                    break;
                case "ORACLE":
                    statement = DatabaseType.ORACLE;
                    break;
            }
            return statement;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
