﻿using Foci.Orbital.Adapters.Database.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Database.Models
{
    /// <summary>
    /// Properties taken from a configuration file for Database Adapter processes.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class DatabaseAdapterConfiguration
    {
        public const string SQL_JSON_PROPERTY = "sql";

        /// <summary>
        /// The relational operator used to compare selected value with the value in compare to.
        /// </summary>
        [JsonProperty("statement")]
        [JsonConverter(typeof(StringStatementJsonConverter))]
        public Statement Statement { get; internal set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(StringTypeJsonConverter))]
        public DatabaseType DatabaseType { get; internal set; }

        /// <summary>
        /// String representation of SQL statement to be executed.
        /// </summary>
        [JsonProperty(SQL_JSON_PROPERTY)]
        public string Sql { get; internal set; }

        /// <summary>
        /// String representation of the connection credentials to be able to connect to the database.
        /// </summary>
        [JsonProperty("connection")]
        public string Connection { get; internal set; }

        /// <summary>
        /// String representation of the connection credentials to be able to connect to the database.
        /// </summary>
        [JsonProperty("parameters")]
        public Dictionary<string, string> Parameters { get; internal set; } = new Dictionary<string, string>();
    }
}
