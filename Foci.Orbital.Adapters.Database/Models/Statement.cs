﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Foci.Orbital.Adapters.Database.Models
{
    /// <summary>
    /// Enum containing all types of statements that are able to be executed.
    /// </summary>
    public enum Statement
    {
        UNDEFINED = 0,
        QUERY,
        EXECUTE
    }
}
