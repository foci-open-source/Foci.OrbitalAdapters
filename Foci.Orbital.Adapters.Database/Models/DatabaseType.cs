﻿namespace Foci.Orbital.Adapters.Database.Models
{
    /// <summary>
    /// Enum containing all supported databases
    /// </summary>
    public enum DatabaseType
    {
        UNDEFINED,
        SQLITE,
        MYSQL,
        POSTGRESQL,
        MSSQL,
        ORACLE
    }
}
