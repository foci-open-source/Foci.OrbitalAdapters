﻿using Foci.Orbital.Adapters.Database.DatabaseClients;
using Foci.Orbital.Adapters.Database.DatabaseClients.Interfaces;
using Foci.Orbital.Adapters.Database.DBClients;
using Foci.Orbital.Adapters.Database.DBClients.Interfaces;
using Foci.Orbital.Adapters.Database.Factories.Interfaces;
using Foci.Orbital.Adapters.Database.Models;


namespace Foci.Orbital.Adapters.Database.Factories
{
    /// <summary>
    /// The factory class to create database client.
    /// </summary>
    public class DatabaseClientFactory : IDatabaseClientFactory
    {
        /// <inheritdoc />
        public IDatabaseClient getClient(DatabaseType databaseType, string connectionString)
        {
            switch (databaseType)
            {
                case DatabaseType.MYSQL:
                    return new MySqlClient(connectionString);
                case DatabaseType.SQLITE:
                    return new SQLiteClient(connectionString);
                case DatabaseType.POSTGRESQL:
                    return new PostgreSQLClient(connectionString);
                case DatabaseType.MSSQL:
                    return new MSSQLClient(connectionString);
                case DatabaseType.ORACLE:
                    return new OracleClient(connectionString);
                default:
                    return null;
            }
        }
    }
}
