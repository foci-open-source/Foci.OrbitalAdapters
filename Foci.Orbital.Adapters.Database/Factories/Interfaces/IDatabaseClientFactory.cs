﻿using Foci.Orbital.Adapters.Database.DBClients.Interfaces;
using Foci.Orbital.Adapters.Database.Models;

namespace Foci.Orbital.Adapters.Database.Factories.Interfaces
{
    public interface IDatabaseClientFactory
    {
        /// <summary>
        /// The method to create database client based on database type and connection string.
        /// </summary>
        /// <param name="databaseType">The type of the database.</param>
        /// <param name="connectionString">The connection string to connect to the corresponding database.</param>
        /// <returns>The connected database client.</returns>
        IDatabaseClient getClient(DatabaseType databaseType, string connectionString);
    }
}
