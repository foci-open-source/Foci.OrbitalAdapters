﻿using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Database.Models;
using System.Collections.Generic;

namespace Foci.Orbital.Adapters.Database.DBClients.Interfaces
{
    public interface IDatabaseClient
    {
        /// <summary>
        /// The execution method of running sql string with dynamic parameters.
        /// </summary>
        /// <param name="statement">The type of the sql statement</param>
        /// <param name="sql">The sql string will be run</param>
        /// <param name="parameters">The parameters will be mapped into the sql statement.
        /// <returns>The payload of the sql statement execution or the fault if execution failed.</returns>
        Payload Execute(Statement statement, string sql, Dictionary<string, string> parameters);
    }
}
