﻿using Dapper;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Database.DBClients.Interfaces;
using Foci.Orbital.Adapters.Database.Models;
using Newtonsoft.Json;
using NLog;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Database.DatabaseClients
{
    /// <summary>
    /// PostgresSQL database client.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PostgreSQLClient : IDatabaseClient
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        private static readonly FaultFactory faultFactory = new FaultFactory();

        private readonly string connectionString;

        /// <inheritdoc />
        public PostgreSQLClient(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <inheritdoc />
        public Payload Execute(Statement statement, string sql, Dictionary<string, string> parameters)
        {
            string error;

            if (statement == Statement.UNDEFINED || string.IsNullOrEmpty(sql))
            {
                error = string.Format("Unable to run the {0} statement: {1}", statement, sql);
                log.Error(error);
                return CreateFault(error);
            }

            DynamicParameters dynamicParameters = new DynamicParameters();
            foreach (var parameter in parameters)
            {
                dynamicParameters.Add(parameter.Key, parameter.Value);
            }

            try
            {
                using (var connection = new NpgsqlConnection(connectionString))
                {
                    if (statement == Statement.QUERY)
                    {
                        var queryResult = connection.Query(sql, dynamicParameters);
                        return Payload.Create(JsonConvert.SerializeObject(queryResult, Formatting.Indented));
                    }
                    else if (statement == Statement.EXECUTE)
                    {
                        var executeResult = connection.Execute(sql, dynamicParameters).ToString();
                        return Payload.Create(string.Format("{0} rows affected", executeResult));
                    }
                    else
                    {
                        error = string.Format("Unable to run the {0} statement: {1}", statement, sql);
                        log.Error(error);
                        return CreateFault(error);
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                log.Error(error);
                return CreateFault(error);
            }
        }

        //Create fault payload with the given message as exception message
        private Payload CreateFault(string message)
        {
            var faultList = new List<Fault>();
            var exception = new OrbitalAdapterException(message);
            var fault = faultFactory.CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList);
        }
    }
}
