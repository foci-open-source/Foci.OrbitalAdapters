using Foci.Orbital.Adapters.Contract;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Database.DBClients.Interfaces;
using Foci.Orbital.Adapters.Database.Factories;
using Foci.Orbital.Adapters.Database.Factories.Interfaces;
using Foci.Orbital.Adapters.Database.Models;
using Newtonsoft.Json.Linq;
using NLog;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Foci.Orbital.Adapters.Database.Services
{
    /// <summary>
    /// Adapter for database response
    /// </summary>
    public class DatabaseAdapter : BaseAdapter
    {
        private const string SQL_VALIDATION_REGEX = @"{{\s?([^}]*)\s?}}";

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        private static readonly FaultFactory faultFactory = new FaultFactory();

        private readonly IDatabaseClientFactory dbClientFactory;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public DatabaseAdapter() : this(new DatabaseClientFactory()) { }

        /// <summary>
        /// Constructor will receive a dbClientFactory instance.
        /// </summary>
        /// <param name="dbClientFactory">The database client factory to create database client.</param>
        internal DatabaseAdapter(IDatabaseClientFactory dbClientFactory)
        {
            this.dbClientFactory = dbClientFactory;
        }

        /// <inheritdoc />
        public override void SendAsync(AdapterRequest request)
        {
            GetPayload(request);
        }

        /// <inheritdoc />
        public override Payload SendSync(AdapterRequest request)
        {
            return GetPayload(request);
        }

        /// <summary>
        /// Retrieve response message or the exception message from the configuration and package it in payload
        /// </summary>
        /// <param name="request">The object containing request information</param>
        /// <returns>Payload containing a message or a fault</returns>
        private Payload GetPayload(AdapterRequest request)
        {
            string error;
            if (request == null)
            {
                error = string.Format("Adapter request cannot be null");
                log.Error("Send: {0}", error);
                return CreateFault(error);
            }

            var sql = ExtractSQL(request.AdapterConfiguration);
            var sqlValidation = new Regex(SQL_VALIDATION_REGEX);
            if (!string.IsNullOrEmpty(sql) && sqlValidation.IsMatch(sql))
            {
                error = string.Format("SQL statement cannot contain mustache template");
                log.Error("Send: {0}", error);
                return CreateFault(error);
            }

            var configuration = LoadParamsToConfig<DatabaseAdapterConfiguration>(request.AdapterConfiguration, request.Parameters);
            if (configuration == null)
            {
                error = string.Format("Unable to read adapter configuration");
                log.Error("Send: {0}", error);
                return CreateFault(error);
            }

            var client = GetClientFromConfiguration(configuration);

            if (client == null)
            {
                error = string.Format("Unable to connect to the database {0} with connection string: {1}", configuration.DatabaseType, configuration.Connection);
                log.Error(error);
                return CreateFault(error);
            }

            return client.Execute(configuration.Statement, configuration.Sql, configuration.Parameters);
        }

        /// <summary>
        /// Extract the SQL statement from JSON string
        /// </summary>
        /// <param name="config">The string representation of configuration</param>
        /// <returns>String representation of SQL statement</returns>
        private string ExtractSQL(string config)
        {
            if (string.IsNullOrWhiteSpace(config))
            {
                log.Warn("ExtractSQL: The string representation of adapter configuration cannot be empty");
                return null;
            }

            var sql = JObject.Parse(config)[DatabaseAdapterConfiguration.SQL_JSON_PROPERTY];
            return sql?.ToString();
        }

        /// <summary>
        /// The method to get the database client based on the configuration.
        /// </summary>
        /// <param name="configuration">The database adapter configuration</param>
        /// <returns>The database client specified by the database adapter configuration.</returns>
        private IDatabaseClient GetClientFromConfiguration(DatabaseAdapterConfiguration configuration)
        {
            if (string.IsNullOrWhiteSpace(configuration.Connection))
            {
                return null;
            }

            return dbClientFactory.getClient(configuration.DatabaseType, configuration.Connection);
        }

        //Create fault payload with the given message as exception message
        private Payload CreateFault(string message)
        {
            var faultList = new List<Fault>();
            var exception = new OrbitalAdapterException(message);
            var fault = faultFactory.CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList);
        }
    }
}
