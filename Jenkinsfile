pipeline {
    agent any

    environment {
        String nugetHome = "nuget"
        String dotnetHome = "dotnet"
        String solution = "Foci.OrbitalBus.Adapters.sln"
        String fetchUrl = "${env.global_AdaptersFetchUrl}"
        String pushUrl = "${env.global_AdaptersPushUrl}"
        String buildNumber = "${env.BUILD_NUMBER}"

    }

    options {
        timeout(time: 20, unit: 'MINUTES')
        gitlabBuilds(builds: ["jenkinsfile-clean", "checkout", "build", "unit-test", "update-artifacts"])
    }

    stages{
        stage('jenkinsfile-clean'){
            steps {
                deleteDir()
            }
            post {
                success {
                    echo 'posting success to GitLab'
                        updateGitlabCommitStatus(name: 'jenkinsfile-clean', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                        updateGitlabCommitStatus(name: 'jenkinsfile-clean', state: 'failed')
                }
            }
        }

        stage('checkout'){
            steps {
                checkout(
                    [$class: 'GitSCM', branches: [[name: 'master']], 
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [], 
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: 'Jenkins_deploy_private_key', url: 'git@gitlab.com:foci-open-source/Foci.OrbitalAdapters.git']]])
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'checkout', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'checkout', state: 'failed')
                }
            }
        }

        stage('build'){
            steps {
                // Restore packages and build.
                sh nugetHome + ' restore ' + solution
                sh dotnetHome + ' build ' + solution
                archiveArtifacts allowEmptyArchive: true, artifacts: '**/*.dll', onlyIfSuccessful: true
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'build', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'build', state: 'failed')
                }
            }
        }

        stage('unit-test'){
            steps {
                sh dotnetHome + 'test \
                    ./Foci.Orbital.Adapters.Rest.Tests \
                    -v n /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Exclude="[*]$AGENT.*" | tee foci_orbital_adapters_rest_tests_report.txt'
            }
            post {
                success {
                    echo 'posting success to GitLab'
                        updateGitlabCommitStatus(name: 'unit-test', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                        updateGitlabCommitStatus(name: 'unit-test', state: 'failed')
                }
            }
        }

        stage('update-artifacts'){
            steps {
                sh 'rm -rf /home/jenkins/Foci.OrbitalBus.Adapters.Rest'
                sh 'mkdir -p /home/jenkins/Foci.OrbitalBus.Adapters.Rest'
                sh 'cp -r ./Foci.Orbital.Adapters.Rest/bin/Debug/* /home/jenkins/Foci.OrbitalBus.Adapters.Rest'
                sh 'mkdir -p /home/jenkins/Compressed/Adapters.Rest/' + buildNumber
                sh 'tar -zcf /home/jenkins/Compressed/Adapters.Rest/' + buildNumber + '/Foci.Orbital.Adapters.Rest.tar.gz -C /home/jenkins Foci.OrbitalBus.Adapters.Rest'
                sh 'zip -rj /home/jenkins/Compressed/Adapters.Rest/' + buildNumber + '/Foci.Orbital.Adapters.Rest.zip /home/jenkins/Foci.OrbitalBus.Adapters/'
            }
            post {
                success {
                    echo 'posting success to GitLab'
                    updateGitlabCommitStatus(name: 'update-artifacts', state: 'success')
                }
                failure {
                    echo 'postinng failure to GitLab'
                    updateGitlabCommitStatus(name: 'update-artifacts', state: 'failed')
                }
            }
        }
    }

    post{
        always{
            script{
                [$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: '"${env.global_recipients}"', sendToIndividuals: true]
                echo 'posting success to GitLab'
                updateGitlabCommitStatus(name: 'post-build', state: 'success')
            }
        }
    }
}
