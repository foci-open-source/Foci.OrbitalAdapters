﻿using Foci.Orbital.Adapters.Contract;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Rest.Extensions;
using Foci.Orbital.Adapters.Rest.Factories;
using Foci.Orbital.Adapters.Rest.Factories.Interfaces;
using Foci.Orbital.Adapters.Rest.Models;
using NLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;

namespace Foci.Orbital.Adapters.Rest.Services
{
    /// <summary>
    /// Adapter for REST calls
    /// </summary>
    public class RestAdapter : BaseAdapter
    {
        private static readonly string jsonContentType = "application/json";
        private static readonly string xmlContentType = "application/xml";

        private static readonly FaultFactory faultFactory = new FaultFactory();
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        private readonly IRestClientFactory restClientFactory;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public RestAdapter() : this(new RestClientFactory()) { }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="restClientFactory">RestClient Factory object to build the REST client request.</param>
        internal RestAdapter(IRestClientFactory restClientFactory)
        {
            this.restClientFactory = restClientFactory;
        }

        /// <inheritdoc />
        public override void SendAsync(AdapterRequest request)
        {
            var response = default(IRestResponse);
            var configuration = LoadParamsToConfig<RestAdapterConfiguration>(request.AdapterConfiguration, request.Parameters);
            if (configuration == null)
            {
                log.Error("SendAsync: Unable to read service configuration at {0}; Message was not sent", request.AdapterConfiguration);
                return;
            }

            string username = string.Empty, password = string.Empty;
            if (configuration.Authorization != null && configuration.Authorization.BasicHttp != null)
            {
                username = configuration.Authorization.BasicHttp.Username;
                password = configuration.Authorization.BasicHttp.Password;
            }

            if (!string.IsNullOrEmpty(request.Message))
            {
                response = (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)) ?
                                restClientFactory.CreateSecureRestClient(configuration.Uri, username, password)
                                            .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                            .AddHeaders(configuration.Headers)
                                            .Execute(request.Message) :
                                restClientFactory.CreateRestClient(configuration.Uri)
                                            .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                            .AddHeaders(configuration.Headers)
                                            .Execute(request.Message);
            }

            else
            {
                response = (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)) ?
                                   restClientFactory.CreateSecureRestClient(configuration.Uri, username, password)
                                                .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                                .AddHeaders(configuration.Headers)
                                                .Execute() :
                                  restClientFactory.CreateRestClient(configuration.Uri)
                                                    .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                                    .AddHeaders(configuration.Headers)
                                                    .Execute();
            }
            ValidateResponse(response);
        }



        /// <inheritdoc />
        public override Payload SendSync(AdapterRequest request)
        {
            var response = default(IRestResponse);
            var configuration = LoadParamsToConfig<RestAdapterConfiguration>(request.AdapterConfiguration, request.Parameters);
            if (configuration == null)
            {
                var faultList = new List<Fault>();
                var message = string.Format("Unable to read service configuration at {0}; Message was not sent", request.AdapterConfiguration);
                log.Error("Send: " + message);
                var exception = new OrbitalAdapterException(message);
                var fault = faultFactory.CreateFault(exception);
                faultList.Add(fault);
                return Payload.Create(faultList);
            }

            string username = string.Empty, password = string.Empty;
            if (configuration.Authorization != null && configuration.Authorization.BasicHttp != null)
            {
                username = configuration.Authorization.BasicHttp.Username;
                password = configuration.Authorization.BasicHttp.Password;
            }

            if (string.IsNullOrEmpty(request.Message))
            {
                response = (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)) ?
                                    restClientFactory.CreateSecureRestClient(configuration.Uri, username, password)
                                                .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                                .AddHeaders(configuration.Headers)
                                                .Execute() :
                                   restClientFactory.CreateRestClient(configuration.Uri)
                                                .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                                .AddHeaders(configuration.Headers)
                                                .Execute();
            }
            else
            {
                response = (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)) ?
                restClientFactory.CreateSecureRestClient(configuration.Uri, username, password)
                                            .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                            .AddHeaders(configuration.Headers)
                                            .Execute(request.Message) :
                                            restClientFactory.CreateRestClient(configuration.Uri)
                                            .CreateRequest(configuration.Endpoint, CreateMethodEnumFromString(configuration.Method))
                                            .AddHeaders(configuration.Headers)
                                            .Execute(request.Message);
            }
            return CreatingAdapterResponsePayload(response);
        }

        #region Private Members
        /// <summary>
        /// Translates a string to the RestSharp method enum.
        /// </summary>
        /// <param name="method">The method as a string.</param>
        /// <returns>The RestSharp compatible enum value.</returns>
        private static Method CreateMethodEnumFromString(string method)
        {
            if (string.IsNullOrWhiteSpace(method))
            {
                log.Error("CreateMethodEnumFromString: Unable to get method type from input, it is null or empty, defaulting method type to GET");
                return Method.GET;
            }

            return (Method)Enum.Parse(typeof(Method), method);
        }

        /// <summary>
        /// Validates response status code
        /// </summary>
        /// <param name="response">The response to validate</param>
        /// <returns cref="IRestResponse">The validated response</returns> 
        private void ValidateResponse(IRestResponse response)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                if (response.ErrorException == null)
                {
                    log.Error("ValidateResponse: An error has occurred: {0} \nContent: {1}", response.StatusDescription, response.Content);
                }
                else
                {
                    log.Error(response.ErrorException, "ValidateResponse: An error has occurred: {0} \nContent: {1}", response.StatusDescription, response.Content);
                }

            }
        }

        /// <summary>
        /// Basic Deserializing function for different content types.
        /// </summary>
        /// <typeparam name="T"> The type to deserialize to.</typeparam>
        /// <param name="response"> The Rest Sharp HTTP Response returned after the request is sent.</param>
        /// <returns>The Payload-wrapped adapter response containing all the necessary information.</returns>
        private Payload CreatingAdapterResponsePayload(IRestResponse response)
        {
            var faultList = new List<Fault>();
            if (response.IsCompleted())
            {
                return CreateSuccessResponse(response);
            }
            else if (response.ErrorException == null)
            {
                return CreateFailureResponse(response);
            }

            var message = "There was a network issue while trying to send the message";

            log.Warn("CreatingAdapterResponse: " + message);

            var exception = new OrbitalAdapterException(message, response.ErrorException);
            var fault = faultFactory.CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList);
        }

        /// <summary>
        /// Creates a failure response for a given HTTP response.
        /// </summary>
        /// <param name="response">The Rest Sharp HTTP Response returned after the request is sent.</param>
        /// <returns>The Payload-wrapped adapter response containing all the necessary information.</returns>
        private static Payload CreateFailureResponse(IRestResponse response)
        {
            var faultList = new List<Fault>();
            var responseHeaders = new PayloadHeaders(response.ToDictionary());

            var faultMessage = string.Concat("Https status code: ",
                                             response.StatusCode,
                                             " Https status code description: ",
                                             response.StatusDescription);

            log.Warn("CreateFailureResponse: " + faultMessage);

            var exception = new OrbitalAdapterException(faultMessage);
            var fault = faultFactory.CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList, responseHeaders);
        }

        /// <summary>
        /// Create a successful response payload from a RestSharp response.
        /// </summary>
        /// <param name="response">The Rest Sharp HTTP Response returned after the request is sent.</param>
        /// <returns>The Payload-wrapped adapter response containing all the necessary information.</returns>
        private static Payload CreateSuccessResponse(IRestResponse response)
        {
            var responseHeaders = new PayloadHeaders(response.ToDictionary());
            var faultList = new List<Fault>();

            if (response.ContentType.IndexOf(jsonContentType) != -1)
            {
                return Payload.Create(response.Content, responseHeaders);
            }

            string message = (response.ContentType.IndexOf(xmlContentType) != -1) ?
                "XML is not implemented at this time." :
                $"Unsupported content type: {response.ContentType}";

            log.Warn("CreateSuccessResponse: " + message);

            var exception = new OrbitalAdapterException(message);
            var fault = faultFactory.CreateFault(exception);
            faultList.Add(fault);
            return Payload.Create(faultList, responseHeaders);
        }
        #endregion
    }
}
