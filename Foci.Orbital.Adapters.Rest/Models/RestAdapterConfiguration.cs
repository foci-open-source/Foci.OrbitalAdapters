﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Rest.Models
{
    /// <summary>
    /// Properties taken from a configuration file for Rest Adapter processes.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestAdapterConfiguration
    {
        /// <summary>
        /// The Uri property represents the uri the RestSharper client is going to hit.
        /// </summary>
        /// <value>
        /// The Uri property gets/sets the value of the string representing the uri the RestSharper client is going to hit.
        /// </value>
        [JsonProperty("uri")]
        public string Uri { get; set; }


        /// <summary>
        /// The Endpoint property represents the Endpoint the RestSharper request is going to hit.
        /// </summary>
        /// <value> 
        /// The Endpoint property gets/sets the value of the string representing the Endpoint the RestSharper request is going to hit.
        /// </value>
        [JsonProperty("endpoint")]
        public string Endpoint { get; set; }


        /// <summary>
        /// The Method property represents the type of RestSharper request.
        /// </summary>
        /// <value>
        /// The Method property gets/sets the value of the string representing the type of RestSharper request. For example, GET method.
        /// </value>
        [JsonProperty("method")]
        public string Method { get; set; }

        /// <summary>
        /// The string representation of consumer username
        /// </summary>
        [JsonProperty("authorization")]
        public Authorization Authorization { get; set; }

        /// <summary>
        /// The Headers property represents a list of Header objects for the RestSharper client request headers. 
        /// </summary>
        /// <value>
        /// The Headers property get/sets the value of the headers for the RestSharper client request headers.
        /// </value>
        [JsonProperty("headers")]
        public List<Header> Headers { get; set; }
    }

    /// <summary>
    /// Properties taken from a configuration file for RestSharp request headers. 
    /// </summary>
    public class Header
    {
        /// <summary>
        /// The Key property represents the name of the header to be added to the RestSharper request. 
        /// </summary>
        /// <value>
        /// The Key property get/sets the string value of the name of the header to be added to the RestSharper request.
        /// </value>
        [JsonProperty("key")]
        public string Key { get; set; }


        /// <summary>
        /// The Value property represents the value of the header to be added to the RestSharper request.
        /// </summary>
        /// <value>
        /// The Value property get/sets the string value of the header's value to be added to the RestSharper request.
        /// </value>
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
