﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Rest.Models
{
    [ExcludeFromCodeCoverage]
    public class Authorization
    {
        /// <summary>
        /// Basic username and password authentication
        /// </summary>
        [JsonProperty("basicHttp")]
        public BasicHttp BasicHttp { get; set; }
    }

    public class BasicHttp
    {
        /// <summary>
        /// String representation of username for authorization
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }

        /// <summary>
        /// String representation of password for authorization
        /// </summary>
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
