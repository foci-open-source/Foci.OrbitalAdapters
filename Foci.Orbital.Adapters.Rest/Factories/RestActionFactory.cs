﻿using Foci.Orbital.Adapters.Rest.Extensions;
using Foci.Orbital.Adapters.Rest.Factories.Interfaces;
using RestSharp;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Rest.Factories
{
    /// <summary>
    /// Chaining object to build out request
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestActionFactory : IRestActionFactory
    {
        private readonly IRestClient client;

        /// <summary>
        /// Default constructor requiring RestSharp IRestClient object.
        /// </summary>
        /// <param name="client"></param>
        public RestActionFactory(IRestClient client)
        {
            this.client = client;
        }

        /// <inheritdoc />
        public IRestRequestFactory CreateRequest(string resource, Method method)
        {
            var request = new RestRequest(resource, method)
            {
                JsonSerializer = new JsonSerializer()
            };
            return new RestRequestFactory(this.client, request);
        }
    }
}
