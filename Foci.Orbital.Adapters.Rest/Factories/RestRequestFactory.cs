﻿using Foci.Orbital.Adapters.Rest.Factories.Interfaces;
using Foci.Orbital.Adapters.Rest.Models;
using NLog;
using RestSharp;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Foci.Orbital.Adapters.Rest.Factories
{
    /// <summary>
    /// Chaining Object to execute the request against the client
    /// </summary>
    public class RestRequestFactory : IRestRequestFactory
    {

        #region Private Members
        private readonly IList<string> jsonContentTypes = new ReadOnlyCollection<string>(new List<string>()
        {
            "application/json",
            "text/json"
        });
        private readonly IRestClient client;
        private bool isBodyJson;
        private readonly IRestRequest request;
        private string contentType = "application/json; charset=utf-8";

        private readonly ILogger log = LogManager.GetCurrentClassLogger();
        #endregion

        /// <summary>
        /// Default Constructor requiring an IRestClient and IRestRequest from RestSharp.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="request"></param>
        public RestRequestFactory(IRestClient client, IRestRequest request)
        {
            this.client = client;
            this.request = request;
        }

        /// <summary>
        /// Add parameter to the rest call.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to use as a key.</param>
        /// <param name="value">The value to add against the parameter key.</param>
        /// <param name="parameterType">THe type of the parameter to add.</param>
        /// <returns>This factory.</returns>
        public IRestRequestFactory AddParameter(string parameterName, object value, ParameterType parameterType)
        {
            this.request.AddParameter(parameterName, value, parameterType);
            return this;
        }

        #region Headers
        /// <inheritdoc />
        public IRestRequestFactory AddHeaders(IEnumerable<Header> headers)
        {
            if (headers == null)
            {
                log.Warn("AddHeaders: No headers defined");
                return this;
            }

            foreach (var header in headers)
            {
                this.AddHeader(header.Key, header.Value);
            }

            return this;
        }

        /// <summary>
        /// Adds header to the rest request.
        /// </summary>
        /// <param name="key">The header key.</param>
        /// <param name="value">The value to add against the key.</param>
        /// <returns>This factory.</returns>
        public IRestRequestFactory AddHeader(string key, string value)
        {
            if (key == "Content-Type")
            {
                if (jsonContentTypes.Contains(value))
                {
                    this.isBodyJson = true;
                }
                this.contentType = value;
            }

            this.request.AddHeader(key, value);
            return this;
        }
        #endregion

        /// <inheritdoc />
        public IRestResponse Execute()
        {
            var response = this.client.Execute(this.request);
            return response;
        }

        /// <inheritdoc />
        public IRestResponse Execute(string message)
        {
            log.Info("Execute: Sending message to endpoint");
            request.AddParameter(this.contentType, message, ParameterType.RequestBody);
            return this.Execute();
        }
    }
}
