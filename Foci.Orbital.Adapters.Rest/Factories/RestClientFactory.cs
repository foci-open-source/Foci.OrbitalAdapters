using Foci.Orbital.Adapters.Rest.Factories.Interfaces;
using NLog;
using RestSharp;
using RestSharp.Authenticators;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Rest.Factories
{
    /// <summary>
    /// First Class to build IRestClient
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RestClientFactory : IRestClientFactory
    {
        private static IRestClient client;

        private readonly ILogger log = LogManager.GetCurrentClassLogger();

        /// <inheritdoc />
        public IRestActionFactory CreateRestClient(string uri)
        {
            if (string.IsNullOrWhiteSpace(uri))
            {
                log.Warn("CreateRestClient: The URI cannot be null or empty");
            }

            client = new RestClient(uri);

            return new RestActionFactory(client);
        }

        /// <inheritdoc />
        public IRestActionFactory CreateSecureRestClient(string uri, string consumerUsername, string consumerPassword)
        {
            if (string.IsNullOrWhiteSpace(uri))
            {
                log.Warn("CreateSecureRestClient: The URI cannot be null or empty");
            }

            if (consumerUsername == null)
            {
                log.Warn("CreateSecureRestClient: The username cannot be null");
            }

            if (consumerPassword == null)
            {
                log.Warn("CreateSecureRestClient: The password cannot be null");
            }

            client = new RestClient(uri)
            {
                Authenticator = new SimpleAuthenticator("username", consumerUsername, "password", consumerPassword)
            };

            return new RestActionFactory(client);
        }
    }
}
