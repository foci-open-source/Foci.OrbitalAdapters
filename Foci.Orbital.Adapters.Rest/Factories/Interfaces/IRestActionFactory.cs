﻿using RestSharp;

namespace Foci.Orbital.Adapters.Rest.Factories.Interfaces
{
    public interface IRestActionFactory
    {
        /// <summary>
        /// The factory that creates a RestRequestFactory. JsonSerializer is created here in
        /// the RestRequest and we pass this to the RestRequestFactory.
        /// </summary>
        /// <param name="resource">The URI to hit.</param>
        /// <param name="method">The method type to execute.</param>
        /// <returns>The factory to send requests.</returns>
        IRestRequestFactory CreateRequest(string endpoint, Method method);
    }
}
