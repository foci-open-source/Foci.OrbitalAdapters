﻿namespace Foci.Orbital.Adapters.Rest.Factories.Interfaces
{
    public interface IRestClientFactory
    {
        /// <summary>
        /// Returns a RestActionFactory. A client is created at this point and passed to
        /// the action factory.
        /// </summary>
        /// <param name="uri">The URI of the client.</param>
        /// <returns>The factory to perform REST actions.</returns>
        IRestActionFactory CreateRestClient(string uri);

        /// <summary>
        /// Returns a RestActionFactory. A client with an http basic authenticator is created at this point and passed to
        /// the action factory.
        /// </summary>
        /// <param name="uri">The URI of the client.</param>
        /// <returns>The factory to perform REST actions.</returns>
        IRestActionFactory CreateSecureRestClient(string uri, string consumerUsername, string consumerPassword);
    }
}
