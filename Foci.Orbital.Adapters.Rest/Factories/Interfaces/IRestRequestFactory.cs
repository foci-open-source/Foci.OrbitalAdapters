﻿using Foci.Orbital.Adapters.Rest.Models;
using RestSharp;
using System.Collections.Generic;

namespace Foci.Orbital.Adapters.Rest.Factories.Interfaces
{
    public interface IRestRequestFactory
    {
        /// <summary>
        /// Call the internal Add Header method for all headers.
        /// </summary>
        /// <param name="headers">The headers to add.</param>
        /// <returns>This factory.</returns>
        IRestRequestFactory AddHeaders(IEnumerable<Header> headers);

        /// <summary>
        /// Calls the execute method and with the private RestRequest.
        /// </summary>
        /// <returns>The RestSharp response.</returns>
        IRestResponse Execute();

        /// <summary>
        /// Adds the message to the body an then calls Execute with out parameters.
        /// </summary>
        /// <param name="message">The message to add as the body.</param>
        /// <returns>The RestSharp response.</returns>
        IRestResponse Execute(string message);
    }
}